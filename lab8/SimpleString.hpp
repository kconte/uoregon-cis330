#include <iostream>
#include <string>


class SimpleString {
    public:
        SimpleString();
        SimpleString(std::string str);
        void setString(std::string str);    
        std::string getString();
    
        // All operator overloading member functions.
        SimpleString& operator=(const SimpleString & s);
        SimpleString& operator=(const char * s);
        SimpleString operator+(const SimpleString & s);

        // Pre-increment and pre-decrement. 
        SimpleString& operator++();
        SimpleString& operator--();

        /* Post-increment and post-decrement */
        SimpleString operator++(int);
        SimpleString operator--(int);

        // Operator overloading friend functions.
        friend std::ostream& operator<<(std::ostream & out, const SimpleString & s);
        friend std::istream& operator>>(std::istream & in, SimpleString & s);

    private:
         std::string input;
};
