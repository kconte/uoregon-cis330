#include "SimpleString.hpp"

// Include other headers as necessary
using namespace std;

int main(int argc, const char *argv[])
{
    SimpleString s1{"Kevin "}, s2 {"Conte"}, s3, s4, s5;

    cout << "Starting Strings: s1 = \"" << s1 << "\", s2 = \"" << s2 << "\"" << endl << endl;

    cout << "Assignment (=)" << endl;
    cout << "Before assignment: s2 = " << s2 << endl;
    s2 = s1;
    cout << "After assignment: s2 = s1 = " << s2 << endl << endl;

    s2 = "Conte";
    s3 = s1 + s2;
    cout << "Concatenation (+)" << endl;
    cout << "s3 = s1 + s2 = " << s3 << endl << endl;

    cout << "Pre-increment (++)" << endl;
    cout << "++s3 = " << ++s3 << endl << endl;

    cout << "Post-increment (++)" << endl;
    s4 = s3++;
    cout << "s4 = s3++ =  " << s4 << endl;
    cout << "s3 = " << s3 << endl << endl;

    cout << "Pre-decrement (--)" << endl;
    cout << "--s3 = " << --s3 << endl << endl;

    cout << "Post-decrement (--)" << endl;
    s4 = s3--;
    cout << "s4 = s3-- = " << s4 << endl;
    cout << "s3 = " << s3 << endl << endl;


    cout << "In-stream (>>)" << endl;
    cout << "Please enter some text: " << endl;
    cin >> s5;
    cout << "s5 = " << s5 << endl << endl;;

    return 0;
}
