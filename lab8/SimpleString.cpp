#include "SimpleString.hpp"

using namespace std;

/* Constructors */

/* Default */
SimpleString::SimpleString() {
    this->input = "Default value";
}

/* Input */
SimpleString::SimpleString(string s) {
    this->input = s;
}

/* Getter */
string SimpleString::getString() {
    return this->input;
}

/* Setter */
void SimpleString::setString(string s) {
    this->input = s;
}

/* operator= */
SimpleString & SimpleString::operator=(const SimpleString & s) {
    this->input = s.input;
    return *this;
}

SimpleString & SimpleString::operator=(const char * s) {
    this->input = s;
    return *this;
}

/* operator+ */
SimpleString SimpleString::operator+(const SimpleString & s) {
    string temp = this->input;
    temp = temp.append(s.input);
    return temp;
}

/* operator++ [pre-increment] */
SimpleString & SimpleString::operator++() {
    this->input.push_back('*');
    return *this;
}

/* operator++(int) [post-increment] */
SimpleString SimpleString::operator++(int) {
    string s = this->input;
    this->input.push_back('*');
    return SimpleString(s);
}

/* operator-- [pre-decrement] */
SimpleString & SimpleString::operator--() {
    this->input.pop_back();
    return *this;
}

/* operator--(int) [post-decrement]*/
SimpleString SimpleString::operator--(int) {
    string s = this->input;
    this->input.pop_back();
    return SimpleString(s);
}

/* operator<< */
ostream & operator<<(ostream & out, const SimpleString & s) {
    out << s.input;
    return out;
}

/* operator>> */
istream & operator>>(istream & in, SimpleString & s) {
    getline(in, s.input);
    return in;
}
