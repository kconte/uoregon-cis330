#include "student.hpp"

#include <iostream>

using namespace std;

int main() {

    Student a; /* Default Constructed */

    vector<pair<string, int>> grades;
    grades.push_back({"CIS 330", 100});
    grades.push_back({"CIS 315", 85});
    grades.push_back({"Math 342", 93});
    grades.push_back({"Math 203", 100});
    Student b("Kevin", "Conte", 19, 'M', grades);
    Student c(b);
    Student d = move(b);

    
    cout << endl << endl;
    cout << "Testing << operator:" << endl;
    cout << a << endl;
    cout << b << endl;
    cout << c << endl;
    cout << d << endl;
    cout << endl << endl;

    cout << "Testing getters:" << endl;
    cout << "c.getFirstName(): " << c.getFirstName() << endl;
    cout << "c.getLastName(): " << c.getLastName() << endl;
    cout << "c.getAge(): " << c.getAge() << endl;
    cout << "c.getGender(): " << c.getGender() << endl;
    cout << endl << endl;

    cout << "Testing setters:" << endl;
    cout << "c.setFirstName(\"First Name\"): " << c.setFirstName("First Name") << endl;
    cout << "c.setLastName(\"Last Name\")" << c.setLastName("Last Name") << endl;
    cout << "c.setAge(0): " << c.setAge(0) << endl;
    cout << "c.setAge(21): " << c.setAge(21) << endl;
    cout << "c.setGender('5'): " << c.setGender('5') << endl;
    cout << "c.setGender('F'): " << c.setGender('F') << endl;
    cout << "New c: " << c << endl << endl << endl;

    return 0;
}
