#ifndef STUDENT_HPP_
#define STUDENT_HPP_

#include <ostream>
#include <string>
#include <map>
#include <vector>
#include <utility>

using namespace std;

class Student {
    public:
        Student(); /* Default Constructor */
        Student(const Student &s); /* Copy Constructor */
        Student(Student &&s); /* Move Constructor */
        Student(string fName, string lName, int age, char gender, vector<pair<string, int>> grades); /* Input Constructor */

        ~Student(); /* Destructor */

        /* Getters */
        string getFirstName();
        string getLastName();
        int getAge();
        char getGender();
        vector<pair<string, int>>& getGrades();

        /* Setters */
        bool setFirstName(string name);
        bool setLastName(string name);
        bool setAge(int a);
        bool setGender(char g);
        bool setGrades(vector<pair<string, int>> grades);

        /* << overload */
        friend ostream &operator<<(ostream &out, const Student &student);

    private:
        /* Data Members */
        string fName;
        string lName;
        int age;
        char gender;
        vector<pair<string, int>> grades;
};

#endif /* STUDENT_HPP_ */
