#include "student.hpp"

#include <iostream>

using namespace std;

/* Default Constructor */
Student::Student() {
    cout << "Default Constructor" << endl;
    /* No values in the data members by default */
    this->fName = "";
    this->lName = "";
    this->age = 0;
    this->gender = 'M';
    this->grades = vector<pair<string, int>>();
}

/* Copy Constructor */
Student::Student(const Student &s) : Student() {
    cout << "Copy Constructor" << endl;
    this->fName = s.fName;
    this->lName = s.lName;
    this->age = s.age;
    this->gender = s.gender;
    this->grades = s.grades;
}

/* Move Constructor */
Student::Student(Student &&s) : Student() {
    cout << "Move Constructor" << endl;
    this->fName = s.fName;
    this->lName = s.lName;
    this->age = s.age;
    this->gender = s.gender;
    this->grades = s.grades;
}

/* Input Constructor */
Student::Student(string fName, string lName, int age, char gender, vector<pair<string, int>> grades) {
    cout << "Input Constructor" << endl;
    this->fName = fName;
    this->lName = lName;
    this->age = age;
    this->gender = gender;
    this->grades = grades;
}

/* Destructor */
Student::~Student() {
    cout << "Destructor: " << this->fName << ' ' << this->lName << endl;
    /* Nothing to do */
}

/* Getters */
string Student::getFirstName() { return this->fName; }
string Student::getLastName() { return this->lName; }
int Student::getAge() { return this->age; }
char Student::getGender() { return this->gender; }
vector<pair<string, int>>& Student::getGrades() { return this->grades; }

/* Setters */
bool Student::setFirstName(string name) {
    this->fName = name;
    return true;
}

bool Student::setLastName(string name) {
    this->lName = name;
    return true;
}

bool Student::setAge(int a) {
    if(a <= 0) {
        return false;
    } else {
        this->age = a;
        return true;
    }
}

bool Student::setGender(char g) {
    if(g != 'M' && g != 'F') {
        return false;
    } else {
        this->gender = g;
        return true;
    }
}

bool Student::setGrades(vector<pair<string, int>> grades) {
    this->grades = grades;
    return true;
}

/* << overload */
ostream &operator<<(ostream &output, const Student &student) {
    if(student.fName == "" || student.lName == "" || student.age == 0 || student.gender == '\0') {
        output << "NULL Student";
        return output;
    }
    output << student.fName << ' ';
    output << student.lName << " (";
    output << student.gender << "), ";
    output << student.age << " yrs, ";

    for(auto it = student.grades.begin(); it != student.grades.end() - 1; ++it) {
        output << (*it).first << ": ";
        if ((*it).second >= 90) { output << "A"; }
        else if((*it).second >= 80) { output << "B"; }
        else if((*it).second >= 70) { output << "C"; }
        else if((*it).second >= 60) { output << "D"; }
        else { output << "F"; }
        output << " | ";
    }


    return output;
}
