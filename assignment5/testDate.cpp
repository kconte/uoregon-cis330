#include <iostream>
#include <fstream>

#include "ioutils.hpp"
#include "cipher.hpp"
#include "date.hpp"

int main(int argc, const char *argv[]) {
    IOUtils io;
    io.openStream(argc, argv);
    string input, encrypted, decrypted;
    input = io.readFromStream();
    cout << "Original text:" << endl << input;

    /* Test Date Cipher */
    DateCipher date;
    encrypted = date.encrypt(input);
    cout << "Encrypted text:" << endl << encrypted;

    decrypted = date.decrypt(encrypted);
    cout << "Decrypted text:" << endl << decrypted;
    
    if(decrypted == input) cout << "Decrypted text matches input!" << endl;
    else {
        cout << "Oops! Decrypted text doesn't match input!" << endl;
        return 1;
    }

    return 0;
}
