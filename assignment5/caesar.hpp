/* 
Author:
    kconte
 */
#ifndef CAESAR_HPP_
#define CAESAR_HPP_

#include "cipher.hpp"

using namespace std;

/* CaesarCipher class */
class CaesarCipher : public Cipher {
    public:
        CaesarCipher();
        CaesarCipher(int key);

        virtual string encrypt(string &text);
        virtual string decrypt(string &text);

    private:
        /* Key used for shifting */
        int key;
};

#endif /* CAESAR_HPP_ */
