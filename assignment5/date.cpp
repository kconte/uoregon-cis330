/* 
Author:
    kconte
*/

#include "cipher.hpp"
#include "date.hpp"

using namespace std;

DateCipher::DateCipher() {
    this->date = "082099"; /* For purposes of assignment, just my birthday */
}

/* Encrypts a string with the cipher's date */
string DateCipher::encrypt(string & text) {
    string encrypted = text;

    char next;

    for(int i = 0; i < text.length(); i++) {
        int shift = date[(i % 6)] - '0';

        /* Case 1: character is lowercase */
        if(text[i] >= 'a' && text[i] <= 'z') {
            /* Force char value to the interval [0, 25] */
            next = text[i] - 'a';

            /* Now, encryption is basically the same as caesar cipher */
            next += shift;
            next %= 26;
            next += 'a';
            encrypted[i] = next;
        }

        /* Case 2: character is uppercase: same logic, different range */
        else if(text[i] >= 'A' && text[i] <= 'Z') {
            char next = text[i] - 'A';
            next += shift;
            next %= 26;
            next += 'A';
            encrypted[i] = next;
        }

    }

    return encrypted;
}

/* Decrypts a string with the cipher's date */
string DateCipher::decrypt(string & text) {
    string decrypted = text;

    for(int i = 0; i < text.length(); i++) {
        int shift = this->date[(i % 6)] - '0';
        /* Case 1: character is lowercase */
        if(text[i] >= 'a' && text[i] <= 'z') {
            char next = text[i] - 'a';
            next -= shift;
            if(next < 0) next += 26;
            next %= 26;
            next += 'a';
            decrypted[i] = next;
        }

        /* Case 2: character is uppercase: same logic, different range */
        else if(text[i] >= 'A' && text[i] <= 'Z') {
            char next = text[i] - 'A';
            next -= shift;
            if(next < 0) next += 26;
            next %= 26;
            next += 'A';
            decrypted[i] = next;
        }
    }

    return decrypted;
}
