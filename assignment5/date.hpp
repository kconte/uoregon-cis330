/* 
Author:
    kconte
 */
#ifndef DATE_HPP_
#define DATE_HPP_

#include "cipher.hpp"

using namespace std;

/* DateCipher Class */
class DateCipher : public Cipher {
    public:
        DateCipher();

        virtual string encrypt(string &text);
        virtual string decrypt(string &text);

    private:
        /* Date used for shifting */
        string date;
};

#endif /* DATE_HPP_ */
