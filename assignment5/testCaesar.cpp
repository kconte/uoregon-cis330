#include <iostream>
#include <fstream>

#include "ioutils.hpp"
#include "cipher.hpp"
#include "caesar.hpp"

int main(int argc, const char *argv[]) {

    IOUtils io;
    io.openStream(argc, argv);
    string input, encrypted, decrypted;
    input = io.readFromStream();
    cout << "Original text:" << endl << input;

    /* Test Caesar Cipher */
    CaesarCipher caesar(50);
    encrypted = caesar.encrypt(input);
    cout << "Encrypted text:" << endl << encrypted;

    decrypted = caesar.decrypt(encrypted);
    cout << "Decrypted text:" << endl << decrypted;

    if(decrypted == input) cout << "Decrypted text matches input!" << endl;
    else {
        cout << "Oops! Decrypted text doesn't match input!" << endl;
        return 1;
    }

    return 0;
}
