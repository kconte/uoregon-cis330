/* 
Author:
    kconte
 */
#include "cipher.hpp"
#include "caesar.hpp"

using namespace std;

/* Default constructor */
CaesarCipher::CaesarCipher() : key(0) {}

/* Constructor that sets key */
CaesarCipher::CaesarCipher(int key) : CaesarCipher() { this->key = key; }

/* Encrypt a string with the cipher's key */
string CaesarCipher::encrypt(string& text) {

    string encrypted = text;

    char next;

    for(int i = 0; i < text.length(); ++i) {
        /* Case 1: character is lowercase or character is ' ' */
        if((text[i] >= 'a' && text[i] <= 'z') || text[i] == ' ') {
            /* If character is space, set it equal to 26 to get proper modular arithmetic */
            if(text[i] == ' ') next = 26;

            /* Otherwise, force the value to the interval [0, 25] */
            else next = text[i] - 'a'; 

            /* Add the key */
            next += this->key;

            /* Mod 27 to get proper alpha index */
            next %= 27;

            /* If mod result is 26, the character is right after 'z' and is thus ' ' */
            if(next == 26) next = ' ';

            /* Otherwise, it is within the lowercase alphabet */
            else next += 'a';

            /* Set the encrypted character */
            encrypted[i] = next;
        }

        /* Case 2: character is uppercase */
        else if(text[i] >= 'A' && text[i] <= 'Z') {
            /* Same logic, different range */
            next = ((text[i] - 'A') + this->key) % 26;
            next += 'A';
            encrypted[i] = next;
        }
    }

    return encrypted;
}

/* Decrypt a string with the cipher's key */
string CaesarCipher::decrypt(string& text) {

    string decrypted = text;

    char next;

    /* Same logic as encrypt, just subtracting the key instead of adding */
    /* Also, if after subtracting, the value is negative, we add the full range of the cipher to get proper indexing */
    for(int i = 0; i < text.length(); ++i) {
        /* Case 1: character is lowercase or ' ' */
        if((text[i] >= 'a' && text[i] <= 'z') || text[i] == ' ') {
            if(text[i] == ' ') next = 26;
            else next = text[i] - 'a';
            next -= this->key;
            next %= 27;
            if(next < 0) next += 27;
            if(next == 26) next = ' ';
            else next += 'a';
            decrypted[i] = next;
        }

        /* Case 2: character is uppercase */
        else if(text[i] >= 'A' && text[i] <= 'Z') {
            char next = text[i] - 'A';
            next -= this->key;
            next %= 26;
            if(next < 0) next += 26;
            next += 'A';
            decrypted[i] = next;
        }
    }

    return decrypted;
}
