#include <stdio.h>
#include "power.h"

int main() {
    printf("2.0 ^ 8.0 = %F\n", power(2.0, 8.0));
    return 0;
}
