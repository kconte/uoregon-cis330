#include "factorial.h"

int factorial(int n) {
    if(n == 0) { return 0; }
    else if(n < 0) { return 0; }

    int result = n;

    for(int i = n - 1; i > 1; i--) {
        result *= i;
    }

    return result;
}
