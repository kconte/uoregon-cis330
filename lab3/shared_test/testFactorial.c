#include "factorial.h"
#include <stdio.h>

int main() {
    printf("10! = %d\n", factorial(10));
    return 0;
}
