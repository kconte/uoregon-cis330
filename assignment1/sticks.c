/*
   Kevin Conte
   Assignment 1
   16 January 2019
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int getUserChoice() {
    /* Choice is initially 0 */
    int choice = 0;
    
    /* Request number of sticks */
    printf("Player 1: How many sticks do you take (1-3)? ");

    /* Validate input
       ==============
       Loop until a number between 1 and 3 (inclusive) is entered
       Non-number inputs and inputs less than 1 or greater than 3 will result in another request
    */
    int valid = scanf("%d", &choice);
    if(choice < 1 || choice > 3) { valid = 0; }
    while(!valid) {
        int gc;
        while((gc = getchar()) != EOF && gc != '\n');

        printf("Player 1: How many sticks do you take (1-3)? ");
        valid = scanf("%d", &choice);
        if(choice < 1 || choice > 3) { valid = 0; }
    }

    return choice;
}

int getComputerChoice(int current_sticks) {
    /* get a pseudo-random integer between 1 and 3 (inclusive) */
    int rand_choice = rand() % 3 + 1;

    /* If only 3 or less sticks remain, take them all. */
    if(current_sticks <= 3) {
        return current_sticks;
    }
    
    /* If 5 sticks remain, only take one to force a win. */
    else if(current_sticks == 5) {
        return 1;
    }

    return rand_choice;
}

int main(int argc, char ** argv) {
    int user, computer, number_sticks;
    /* Ensure that user, computer, and number_sticks are initiallized with a constant value */
    user = computer = number_sticks = 0;

    /* Seed the pseudo-random number generator */
    srand(time(NULL)); /* for reproducible results, you can call srand(1); */

    /* Welcome message */
    printf("Welcome to the game of sticks!\n");

    /* Request number of sticks for the game */
    printf("How many sticks are there on the table initially (10-100)? ");

    /* Validate number_sticks input */
    int valid = scanf("%d", &number_sticks);
    if (number_sticks < 10 || number_sticks > 100) { valid = 0; }
    if(!valid) {
        fprintf(stderr, "Invalid input!\n");
        exit(1);
    }

    /* Main game loop */
    while(1) {
        /* Report number of sticks on the board */
        printf("\nThere are %d sticks on the board.\n", number_sticks);


        /* Get user choice and determine if game is over */
        user = getUserChoice();
        /* If user selects a number greater than or equal to number of sticks, they win */
        /* getUserChoice validates input is on [1,3] */
        if(user >= number_sticks) {
            printf("Computer Loses!\n");
            break;
        }

        /* Otherwise, deduct user's choice from number of sticks and report remaining number */
        else {
            number_sticks -= user;
        }
        printf("\nThere are %d sticks on the board.\n", number_sticks);


        /* Get computer choice and determine if game is over */
        computer = getComputerChoice(number_sticks);
        printf("Computer selects %d.\n", computer);
        if(computer == number_sticks) {
            printf("Computer Wins!\n");
            break;
        } else {
            number_sticks -= computer;
        }
    }

    /* Exit with success code */
    return 0;
}
