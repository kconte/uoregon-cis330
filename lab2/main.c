#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define bool unsigned char
#define true 1
#define false 0

#define ROW 3
#define COL 2

bool arrayEqual(int A[ROW][COL], int B[ROW][COL], int m, int n) {
    return memcmp((void *)A, (void *)B, m * n * sizeof(int)) == 0 ? true: false;
}

bool arrayEqual2(int * A, int * B, int n) {
    return memcmp((void *)A, (void *)B, n * sizeof(int)) == 0 ? true : false;
}

int main() {

    int A[ROW][COL] = { {1,2}, {3,4}, {5,6} };
    int B[ROW][COL] = { {1,2}, {3,4}, {5,6} };

    bool isEqual = arrayEqual(A, B, ROW, COL);

    if(isEqual) { printf("true\n"); }
    else { printf("false\n"); }


    int * C = (int *) malloc(ROW * COL * sizeof(int));
    int * D = (int *) malloc(ROW * COL * sizeof(int));

    isEqual = arrayEqual2(C, D, ROW * COL);
    if(isEqual) { printf("true\n"); }
    else { printf("false\n"); }

    for(int i = 0; i < ROW * COL; i++) {
        C[i] = i;
        D[i] = i * i;
    }

    isEqual = arrayEqual2(C, D, ROW * COL);
    if(isEqual) { printf("true\n"); }
    else { printf("false\n"); }

    free(C);
    free(D);

    return 0;
}
