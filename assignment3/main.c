#include "othello.h"
#include <stdio.h>

int main() {
    /* GameBoard used for the remainder of the application */
    GameBoard gameBoard;

    /* Get the board size from the user. */
    int boardSize = 0, valid = 0, gc = 0;
    printf("Please enter the size of the board: ");
    valid = scanf("%d", &boardSize);
    if(boardSize < 3) { valid = 0; }

    /* Invalid inputs trigger another request for board size. */
    /* Invalid inputs include: non-integers and integers < 3 */
    while(!valid) {
        while((gc = getchar()) != EOF && gc != '\n');
        printf("Please enter the size of the board: ");
        valid = scanf("%d", &boardSize);
        if(boardSize < 3) { valid = 0; }
    }

    /* Initialize the GameBoard with the user-provided size */
    initGameBoard(&gameBoard, boardSize);

    /* Assign the current player to be BLACK */
    TileState curPlayer = BLACK;

    /* Main loop of application */
    /* Loop until the game is determined to be over, basiscally until there are no valid moves for the current player */
    while(!gameOver(&gameBoard)) {

        /* Print board state to screen */
        printBoard(&gameBoard);

        /* Retrieve move from the current player */
        getMove(&gameBoard, curPlayer);

        /* Switch curPlayer to its opponent */
        curPlayer = !curPlayer;

        /* Update the board to display the valid moves for the next player */
        updateValidTiles(&gameBoard, curPlayer);
    }


    /* Print a final board state */
    printBoard(&gameBoard);
    printf("\n\n");

    /* Announce the winner of the game */
    announceWinner(&gameBoard);

    /* Clean up any dynamically allocated memory */
    cleanGameBoard(&gameBoard);

    return 0;
}
