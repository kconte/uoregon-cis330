#include "othello.h"
#include <stdio.h>
#include <stdlib.h>

/* Initialize a gameboard with a given width */
void initGameBoard(GameBoard * gameBoard, int width) {
    /* Since the board MUST be a square (by the rules of othello), height == width, and thus we do
       not need a height property
     */
    gameBoard->width = width;

    /* Allocate memory for the gameBoard */
    gameBoard->board = (TileState **) malloc(sizeof(TileState *) * width);

    /* If malloc fails, alert user and exit */
    if(!gameBoard->board) {
        fprintf(stderr, "Couldn't allocate memory for the gameBoard->board!\n");
        exit(1);
    }
    
    /* Allocate memory for each of the rows */
    for(int i = 0; i < width; i++) {
        gameBoard->board[i] = (TileState *) malloc(sizeof(TileState) * width);
        /* If malloc fails, alert user and exit */
        if(!gameBoard->board[i]) {
            fprintf(stderr, "Couldn't allocate memory for the gameBoard->board[%d]!\n", i);
            exit(1);
        }
    }

    /* Ensure that the board is full of DEFAULT tiles. */
    for(int i = 0; i < width; i++) {
        for(int j = 0; j < width; j++) {
            gameBoard->board[i][j] = DEFAULT;
        }
    }

    /* Set the center of the board to the famous checkerboard pattern */
    int center = width / 2;
    gameBoard->board[center - 1][center - 1] = BLACK;
    gameBoard->board[center - 1][center]     = WHITE;
    gameBoard->board[center][center - 1]     = WHITE;
    gameBoard->board[center][center]         = BLACK;

    updateValidTiles(gameBoard, BLACK);
}

/* Free any dynamically allocated memory used by gameBoard */
void cleanGameBoard(GameBoard * gameBoard) {
    /* In our case, the entire board is dynamically allocated */
    for(int i = 0; i < gameBoard->width; i++) {
        free(gameBoard->board[i]);
    }
    free(gameBoard->board);
}

/* Print the board in its current state to stdout */
void printBoard(const GameBoard * gameBoard) {
    printf("\n\n");

    /* Print the key to the players: */
    printf("  . - Invalid tile\n");
    printf("  x - Open tile\n");
    printf("  * - Black tile (Player 1)\n");
    printf("  O - White tile (Player 2)\n");

    printf("   ");
    for(int i = 0; i < gameBoard->width; i++) { printf("%2d ", i + 1); }
    printf("\n");
    for(int i = 0; i < gameBoard->width; i++) {
        printf("%2d ", i + 1);
        for(int j = 0; j < gameBoard->width; j++) {
            switch(gameBoard->board[i][j]) {
                case DEFAULT:
                    printf(" . ");
                    break;
                case OPEN:
                    printf(" x ");
                    break;
                case WHITE:
                    printf(" O ");
                    break;
                case BLACK:
                    printf(" * ");
                    break;
                default:
                    break;
            }
        }
        printf("\n");
    }
    printf("\n");
}

/* Clear the board of any valid moves */
/* Prepares the board for an update for a new player */
void resetValidTiles(GameBoard * gameBoard) {
    int width = gameBoard->width;
    for(int i = 0; i < width; i++) {
        for(int j = 0; j < width; j++) {
            if(gameBoard->board[i][j] == OPEN) {
                gameBoard->board[i][j] = DEFAULT;
            }
        }
    }
}

/* Determine any valid moves available for the player */
void updateValidTiles(GameBoard * gameBoard, const TileState player) {
    resetValidTiles(gameBoard);

    int width = gameBoard->width;

    for(int i = 0; i < width; i++) {
        for(int j = 0; j < width; j++) {

            if(gameBoard->board[i][j] == player) {
                /* Check all around tile for valid moves */

                /* Upper-Left */
                if(i > 1 && j > 1 && gameBoard->board[i - 1][j - 1] == !player) {
                    if(gameBoard->board[i - 2][j - 2] == DEFAULT) {
                        gameBoard->board[i - 2][j - 2] = OPEN;
                    }
                }

                /* Upper */
                if(i > 1 && gameBoard->board[i - 1][j] == !player) {
                    if(gameBoard->board[i - 2][j] == DEFAULT) {
                        gameBoard->board[i - 2][j] = OPEN;
                    }
                }

                /* Upper-Right */
                if(i > 1 && j < width - 2 && gameBoard->board[i - 1][j + 1] == !player) {
                    if(gameBoard->board[i - 2][j + 2] == DEFAULT) {
                        gameBoard->board[i - 2][j + 2] = OPEN;
                    }
                }

                /* Left */
                if(j > 1 && gameBoard->board[i][j - 1] == !player) {
                    if(gameBoard->board[i][j - 2] == DEFAULT) {
                        gameBoard->board[i][j - 2] = OPEN;
                    }
                }

                /* Right */
                if(j < width - 2 && gameBoard->board[i][j + 1] == !player) {
                    if(gameBoard->board[i][j + 2] == DEFAULT) {
                        gameBoard->board[i][j + 2] = OPEN;
                    }
                }

                /* Lower-Left */
                if(i < width - 2 && j > 1 && gameBoard->board[i + 1][j - 1] == !player) {
                    if(gameBoard->board[i + 2][j - 2] == DEFAULT) {
                        gameBoard->board[i + 2][j - 2] = OPEN;
                    }
                }

                /* Lower */
                if(i < width - 2 && gameBoard->board[i + 1][j] == !player) {
                    if(gameBoard->board[i + 2][j] == DEFAULT) {
                        gameBoard->board[i + 2][j] = OPEN;
                    }
                }

                /* Lower-Right */
                if(i < width - 2 && j < width - 2 && gameBoard->board[i + 1][j + 1] == !player) {
                    if(gameBoard->board[i + 2][j + 2] == DEFAULT) {
                        gameBoard->board[i + 2][j + 2] = OPEN;
                    }
                }

            }
        }
    }
}

/* Retrieve a move from a player */
void getMove(GameBoard * gameBoard, const TileState player) {
    int row = 0, col = 0, valid = 0, gc = 0;
    int width = gameBoard->width;

    printf("Player %d, enter row and col: ", player + 1);
    valid = scanf("%d %d", &row, &col);

    if((row < 1 || row > width) || (col < 0 || col > width)) { valid = 0; }

    if(valid && (gameBoard->board[row - 1][col - 1] != OPEN)) { valid = 0; }

    /* Loop until valid input is entered */
    /* If valid input is entered intially, this loop will not execute */
    /* Invalid input includes: row out of bounds, col out of bounds, board[row][col] not a valid move, non-integers */
    while(!valid) {
        while((gc = getchar()) != EOF && gc != '\n');
        printf("Player %d, enter row and col: ", player + 1);
        valid = scanf("%d %d", &row, &col);

        if((row < 1 || row > width) || (col < 0 || col > width)) { valid = 0; }
        
        if(valid && (gameBoard->board[row - 1][col - 1] != OPEN)) { valid = 0; }
    }

    while((gc = getchar()) != EOF && gc != '\n');

    gameBoard->board[row - 1][col - 1] = player;
    applyMove(gameBoard, player, row - 1, col - 1);
}

/* Apply the move provided by the player. */
/* NEVER called by the main method */
/* Should only be called by the getMove method */
void applyMove(GameBoard * gameBoard, const TileState player, const int row, const int col) {
    /* Same idea as updating the valid moves, except this time we only check immediately around
       the tile that was just updated. We have no need to iterate the entire board, when only
       the surrounding tiles are important
    */

    int width = gameBoard->width;

    /* Upper-Left */
    if(row > 1 && col > 1 && gameBoard->board[row - 2][col - 2] == player) {
        if(gameBoard->board[row - 1][col - 1] == !player) {
            gameBoard->board[row - 1][col - 1] = player;
        }
    }

    /* Upper */
    if(row > 1 && gameBoard->board[row - 2][col] == player) {
        if(gameBoard->board[row - 1][col] == !player) {
            gameBoard->board[row - 1][col] = player;
        }
    }

    /* Upper-Right */
    if(row > 1 && col < width - 2 && gameBoard->board[row - 2][col + 2] == player) {
        if(gameBoard->board[row - 1][col + 1] == !player) {
            gameBoard->board[row - 1][col + 1] = player;
        }
    }

    /* Left */
    if(col > 1 && gameBoard->board[row][col - 2] == player) {
        if(gameBoard->board[row][col - 1] == !player) {
            gameBoard->board[row][col - 1] = player;
        }
    }

    /* Right */
    if(col < width - 2 && gameBoard->board[row][col + 2] == player) {
        if(gameBoard->board[row][col + 1] == !player) {
            gameBoard->board[row][col + 1] = player;
        }
    }

    /* Lower-Left */
    if(row < width - 2 && col > 1 && gameBoard->board[row + 2][col - 2] == player) {
        if(gameBoard->board[row + 1][col - 1] == !player) {
            gameBoard->board[row + 1][col - 1] = player;
        }
    }

    /* Lower */
    if(row < width - 2 && gameBoard->board[row + 2][col] == player) {
        if(gameBoard->board[row + 1][col] == !player) {
            gameBoard->board[row + 1][col] = player;
        }
    }

    /* Lower-Right */
    if(row < width - 2 && col < width - 2 && gameBoard->board[row + 2][col + 2] == player) {
        if(gameBoard->board[row + 1][col + 1] == !player) {
            gameBoard->board[row + 1][col + 1] = player;
        }
    }

}

/* Determine if the game is over */
int gameOver(const GameBoard * gameBoard) {
    int width = gameBoard->width;

    /* Loop over all tiles, if a valid tile is found, the game is not over */
    for(int i = 0; i < width; i++) {
        for(int j = 0; j < width; j++) {
            if(gameBoard->board[i][j] == OPEN) {
                return 0;
            }
        }
    }
    return 1;
}

/* Announce the winner of game: player with largest score */
void announceWinner(const GameBoard * gameBoard) {
    int p1Score = 0;
    int p2Score = 0;
    int width = gameBoard->width;

    for(int i = 0; i < width; i++) {
        for(int j = 0; j < width; j++) {
            switch(gameBoard->board[i][j]) {
                case BLACK:
                    p1Score++;
                    break;
                case WHITE:
                    p2Score++;
                    break;
                default:
                    break;
            }
        }
    }

    printf("Player 1 Score: %d\n", p1Score);
    printf("Player 2 Score: %d\n", p2Score);

    if(p1Score > p2Score) { printf("Player 1 wins!\n"); }
    else if(p1Score < p2Score) { printf("Player 2 wins!\n"); }
    else { printf("It's a tie!\n"); }
}
