#ifndef OTHELLO_H_
#define OTHELLO_H_

typedef enum {
    BLACK   = 0, /* i.e. Player: BLACK has that tile */
    WHITE   = 1, /* i.e. Player: WHITE has that tile */
    DEFAULT = 2, /* i.e. not a valid slot to place a tile */
    OPEN    = 3, /* i.e. valid slot to place a tile */
} TileState;

typedef struct {
    int width;          /* width of board */
    TileState ** board; /* Two dimensional array of tiles to represent game board */
} GameBoard;

void initGameBoard(GameBoard * gameBoard, int width);
void cleanGameBoard(GameBoard * gameBoard);
void printBoard(const GameBoard * gameBoard);

void resetValidTiles(GameBoard * gameBoard);
void updateValidTiles(GameBoard * gameBoard, const TileState player);

void getMove(GameBoard * gameBoard, const TileState player);
void applyMove(GameBoard * gameBoard, const TileState player, const int row, const int col);

int gameOver(const GameBoard * gameBoard);
void announceWinner(const GameBoard * gameBoard);

#endif /* OTHELLO_H_ */
