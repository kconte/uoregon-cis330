#include "othello.hpp"
#include <iostream>

int main() {

    /* Get size of board from user */
    std::cout << "Please enter the size of the board: ";
    int width = 0;
    std::cin >> width;

    /* Repeat request until a valid board size is input */
    while(width < 3) {
        /* Clear the stream, to avoid misreading data */
        std::cin.clear();
        std::cin.ignore(16384, '\n');

        /* Ensure the width is reset to 0, preparing for another request */
        width = 0;
        std::cout << "Please enter the size of the board: ";
        std::cin >> width;
    }

    /* Make a new GameBoard with the user-provided width */
    GameBoard gameBoard = GameBoard(width);

    /* First player (curPlayer) is BLACK, per the rules of Othello */
    TileState curPlayer = BLACK;

    /* Loop until no valid moves are available */
    while(!gameBoard.gameOver()) {
        /* Print current game state to stdout */
        gameBoard.print();

        /* Get a move from the current player */
        gameBoard.getMove(curPlayer);

        /* Set the current player to the opposite player */
        curPlayer = (TileState) !curPlayer;

        /* Update the board for valid moves for the new curPlayer */
        gameBoard.updateValidTiles(curPlayer);

        /* Separate game plays by a line (more readable) */
        std::cout << std::string(80, '=') << std::endl;
    }

    /* Print final game state */
    gameBoard.print();

    /* Announce the winner of the game, as well as each player's score */
    gameBoard.announceWinner();

    return 0;
}
