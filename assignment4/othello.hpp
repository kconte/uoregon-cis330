#ifndef OTHELLO_HPP_
#define OTHELLO_HPP_

/* enum for each of the possible states a tile can be in the game */
typedef enum {
    BLACK = 0,
    WHITE = 1,
    INVALID = 2,
    VALID = 3,
} TileState;

/* GameBoard Class, represents the game board for the game of Othello */
class GameBoard {
    public:
        GameBoard() : width(0), board(0x0) {}; /* Default constructor, should not be called by the user */
        GameBoard(int w); /* Constructor that takes a width as an argument */
        ~GameBoard(); /* Destructor, deletes any dynamically allocated memory */
        void print(); /* Print current GameBoard state */
        void updateValidTiles(const TileState player); /* Determine the valid moves for a player */
        void getMove(const TileState player); /* Get a move from a player */
        bool gameOver(); /* Determine if the game is over */
        void announceWinner(); /* Determine the winner of the game and display player scores */
    private:
        int width; /* Width of the board */
        TileState ** board; /* 2-D array, representing the board */
        void resetValidTiles(); /* Reset the board of any valid moves, used when switching players */
        void applyMove(const TileState player, const int row, const int col); /* Apply a move by a player */
};

#endif /* OTHELLO_HPP_ */
