#include "othello.hpp"
#include <iostream>
#include <iomanip>
#include <limits>

GameBoard::GameBoard(int w) : GameBoard() {
    /* Set width of GameBoard */
    this->width = w;

    /* Allocate memory for the new GameBoard */

    this->board = new TileState*[w];
    /* Make sure memory was allocated for the board */
    if(this->board == NULL) {
        std::cerr << "Unable to allocate memory for this->board!" << std::endl;
        exit(1);
    }
    /* Allocate space for each row of the board */
    for(int i = 0; i < w; i++) {
        this->board[i] = new TileState[w];

        /* Make sure memory was allocated for each row */
        if(this->board[i] == NULL) {
            std::cerr << "Unable to allocate memory for this->board[" << i << "]" << std::endl;
            exit(1);
        }
    }

    /* Make sure all of the squares are set to INVALID, by default */
    for(int i = 0; i < w; i++) {
        for(int j = 0; j < w; j++) {
            this->board[i][j] = INVALID;
        }
    }

    /* Set the center of the board to the famous checkerboard */
    int center = w / 2;
    this->board[center - 1][center - 1] = BLACK;
    this->board[center - 1][center]     = WHITE;
    this->board[center][center - 1]     = WHITE;
    this->board[center][center]         = BLACK;

    /* Prepare the board for the first player, BLACK */
    this->updateValidTiles(BLACK);
}

/* Clean any dynamically allocated memory used by a GameBoard */
GameBoard::~GameBoard() {
    /* Avoid repeated memory accesses in the for-loop */
    int w = this->width;

    /* Delete each row */
    for(int i = 0; i < w; i++) {
        delete[] this->board[i];
    }
    /* Delete the over-arching board */
    delete[] this->board;
}

/* Print the board to the screen, with the key */
void GameBoard::print() {
    /* Make sure the board is initialized */
    if(this->board == NULL) {
        std::cerr << "GameBoard not initialized!" << std::endl;
        return;
    }

    /* Print key */
    std::cout << std::endl;
    std::cout << "  . - Invalid Tile" << std::endl;
    std::cout << "  X - Valid Tile" << std::endl;
    std::cout << "  * - Black Tile" << std::endl;
    std::cout << "  O - White Tile" << std::endl;
    std::cout << std::endl;

    /* Avoid repeated memory accesses in the following for-loops */
    int w = this->width;

    /* Print the columns */
    std::cout << "   ";
    for(int i = 0; i < w; i++) { std::cout << std::setw(2) << i + 1 << " "; }
    std::cout << std::endl;

    /* Loop through each row */
    for(int i = 0; i < w; i++) {
        /* For each row, print row index */
        std::cout << std::setw(2) << i + 1 << " ";
        /* Loop through each column, printing appropriate character for the TileState */
        for(int j = 0; j < w; j++) {
            switch(this->board[i][j]) {
                case BLACK:
                    std::cout << " * ";
                    break;
                case WHITE:
                    std::cout << " O ";
                    break;
                case INVALID:
                    std::cout << " . ";
                    break;
                case VALID:
                    std::cout << " X ";
                    break;
            }
        }
        /* Finish the line at the end of each row */
        std::cout << std::endl;
    }

    /* Spacing */
    std::cout << std::endl;
}

/* Clear the board of any valid moves */
/* Prepares the board for an update for a new player */
void GameBoard::resetValidTiles() {
    int w = this->width;
    for(int i = 0; i < w; i++) {
        for(int j = 0; j < w; j++) {
            if(this->board[i][j] == VALID) {
                this->board[i][j] = INVALID;
            }
        }
    }
}


/* Determine any valid moves available for the player */
void GameBoard::updateValidTiles(const TileState player) {
    this->resetValidTiles();

    int w = this->width;

    for(int i = 0; i < width; i++) {
        for(int j = 0; j < width; j++) {
            if(this->board[i][j] == player) {

                /* Upper-Left */
                if(i > 1 && j > 1 && this->board[i - 1][j - 1] == !player) {
                    if(this->board[i - 2][j - 2] == INVALID) {
                        this->board[i - 2][j - 2] = VALID;
                    }
                }

                /* Upper */
                if(i > 1 && this->board[i - 1][j] == !player) {
                    if(this->board[i - 2][j] == INVALID) {
                        this->board[i - 2][j] = VALID;
                    }
                }

                /* Upper-Right */
                if(i > 1 && j < w - 2 && this->board[i - 1][j + 1] == !player) {
                    if(this->board[i - 2][j + 2] == INVALID) {
                        this->board[i - 2][j + 2] = VALID;
                    }
                }

                /* Left */
                if(j > 1 && this->board[i][j - 1] == !player) {
                    if(this->board[i][j - 2] == INVALID) {
                        this->board[i][j - 2] = VALID;
                    }
                }

                /* Right */
                if(j < w - 2&& this->board[i][j + 1] == !player) {
                    if(this->board[i][j + 2] == INVALID) {
                        this->board[i][j + 2] = VALID;
                    }
                }

                /* Lower-Left */
                if(i < w - 2 && j > 1 && this->board[i + 1][j - 1] == !player) {
                    if(this->board[i + 2][j - 2] == INVALID) {
                        this->board[i + 2][j - 2] = VALID;
                    }
                }

                /* Lower */
                if(i < w - 2 && this->board[i + 1][j] == !player) {
                    if(this->board[i + 2][j] == INVALID) {
                        this->board[i + 2][j] = VALID;
                    }
                }

                /* Lower-Right */
                if(i < w - 2 && j < w - 2 && this->board[i + 1][j + 1] == !player) {
                    if(this->board[i + 2][j + 2] == INVALID) {
                        this->board[i + 2][j + 2] = VALID;
                    }
                }

            }
        }
    }
}


/* Get a move from a player for the game */
void GameBoard::getMove(const TileState player) {

    int row = 0, col = 0, valid = 1, w = this->width;
    std::cout << "Player " << player + 1 << ", enter row and col: ";
    /* Read in row and col from cin, separated by whitespace */
    std::cin >> row >> col;

    if((row < 0 || row > w) || (col < 0 || col > w)) { valid = 0; }
    if(valid && (this->board[row - 1][col - 1] != VALID)) { valid = 0; }
    while(!valid) {
        /* Clear out the stdin stream, to avoid reading garbage */
        std::cin.clear();
        std::cin.ignore(16384, '\n');

        /* Re-request the row and col from player */
        std::cout << "Player " << player + 1 << ", enter row and col: ";
        std::cin >> row >> col;
        valid = 1;
        if((row < 0 || row > w) || (col < 0 || col > w)) { valid = 0; }
        if(valid && (this->board[row - 1][col - 1] != VALID)) { valid = 0; }
    }

    /* Clear stdin stream, to avoid reading garbage */
    std::cin.clear();
    std::cin.ignore(16384, '\n');

    /* Adjust row and col indexes, seeing as they are input as one greater than the actual row and col */
    row--;
    col--;
    this->board[row][col] = player;
    this->applyMove(player, row, col);
    std::cout << std::endl;
}

/* Apply a move by a player, given a row and col */
void GameBoard::applyMove(const TileState player, const int row, const int col) {
    int w = this->width;

    /* Upper-Left */
    if(row > 1 && col > 1 && this->board[row - 2][col - 2] == player) {
        if(this->board[row - 1][col - 1] == !player) {
            this->board[row - 1][col - 1] = player;
        }
    }

    /* Upper */
    if(row > 1 && this->board[row - 2][col] == player) {
        if(this->board[row - 1][col] == !player) {
            this->board[row - 1][col] = player;
        }
    }

    /* Upper-Right */
    if(row > 1 && col < w - 2 && this->board[row - 2][col + 2] == player) {
        if(this->board[row - 1][col - 1] == !player) {
            this->board[row - 1][col - 1] = player;
        }
    }

    /* Left */
    if(col > 1 && this->board[row][col - 2] == player) {
        if(this->board[row][col - 1] == !player) {
            this->board[row][col - 1] = player;
        }
    }

    /* Right */
    if(col < w - 2 && this->board[row][col + 2] == player) {
        if(this->board[row][col + 1] == !player) {
            this->board[row][col + 1] = player;
        }
    }

    /* Lower-Left */
    if(row < w - 2 && col > 1 && this->board[row + 2][col - 2] == player) {
        if(this->board[row + 1][col - 1] == !player) {
            this->board[row + 1][col - 1] = player;
        }
    }

    /* Lower */
    if(row < w - 2 && this->board[row + 2][col] == player) {
        if(this->board[row + 1][col] == !player) {
            this->board[row + 1][col] = player;
        }
    }

    /* Lower-Right */
    if(row < w - 2 && col < w - 2 && this->board[row + 2][col + 2] == player) {
        if(this->board[row + 1][col + 1] == !player) {
            this->board[row + 1][col + 1] = player;
        }
    }
}


/* Determine if the game is over, i.e. no more valid moves */
bool GameBoard::gameOver() {
    int w = this->width;
    for(int i = 0; i < w; i++) {
        for(int j = 0; j < w; j++) {
            if(this->board[i][j] == VALID) {
                return false;
            }
        }
    }
    return true;
}

/* Announce the scores of each player, and the winner of the game */
void GameBoard::announceWinner() {
    int blackScore = 0;
    int whiteScore = 0;
    int w = this->width;

    /* Calculate each player's score */
    for(int i = 0; i < w; i++) {
        for(int j = 0; j < w; j++) {
            switch(this->board[i][j]) {
                case BLACK:
                    blackScore++;
                    break;
                case WHITE:
                    whiteScore++;
                    break;
                default:
                    break;
            }
        }
    }

    /* Report player scores */
    std::cout << "BLACK score: " << blackScore << std::endl;
    std::cout << "WHITE score: " << whiteScore << std::endl;

    /* Determine winner */
    if(blackScore > whiteScore) {
        std::cout << "BLACK wins!" << std::endl;
    } else if(blackScore < whiteScore) {
        std::cout << "WHITE wins!" << std::endl;
    } else {
        std::cout << "It's a tie!" << std::endl;
    }
}
