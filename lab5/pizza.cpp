#include <iostream>
#include <ostream>
#include <string>
#include <vector>

using namespace std;

class Pizza {
    public:
        Pizza();
        Pizza(vector<string> toppings);

        friend ostream &operator<<(ostream &output, const Pizza &p);
        friend Pizza &operator+(Pizza & pizza, const string topping);
        friend Pizza &operator+(Pizza & pizza, const string toppings[]);
        friend Pizza &operator+(Pizza & pizza, const vector<string> toppings);
    private:
        vector<string> tops; /* toppings of pizza */
        int diameter;                  /* size of pizza in inches */
};

/* Pizza constructors */
Pizza::Pizza() {
    this->tops = { "Cheese" };
    this->diameter = 12; /* 12 inch diameter, by default */
}

Pizza::Pizza(vector<string> toppings) : Pizza() {
    this->tops.insert(this->tops.end(), toppings.begin(), toppings.end());
}

/* << operator overload */
ostream& operator<<(ostream &output, const Pizza &p) {
    output << "Size: " << p.diameter << "\"";
    output << endl;
    output << "Toppings: " << endl;
    for(int i = 0; i < p.tops.size(); i++) {
        output << " + " << p.tops[i] << endl;
    }
    return output;
}

/* + operator overload */
Pizza &operator+(Pizza &pizza, const string topping) {
    pizza.tops.push_back(topping);
    return pizza;
}

Pizza &operator+(Pizza &pizza, const string toppings[]) {
    return pizza;
}

Pizza &operator+(Pizza &pizza, const vector<string> toppings) {
    pizza.tops.insert(pizza.tops.end(), toppings.begin(), toppings.end());
    return pizza;
}

int main() {

    Pizza p1;
    Pizza p2({"Bacon", "Pineapple"});

    cout << "Default Pizza: " << endl << p1;

    cout << endl << "Constructed Pizza:" << endl << p2;

    p2 = p2 + "Sausage";
    cout << endl << "Constructed Pizza:" << endl << p2;


    vector<string> new_toppings = {"Pepperoni", "Ground Beef"};
    p2 = p2 + new_toppings;
    cout << endl << "Constructed Pizza:" << endl << p2;

    return 0;
}
