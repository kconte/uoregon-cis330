
#include "clock.h"
#include "timer.h"
#include <stdio.h>
#include <unistd.h>

void initTimer(ClockType * clock, int minutes, int seconds) {
    initClock(clock);
    clock->seconds = (minutes * 60) + seconds;
    setClockFmt(clock, "%M:%S");
    return;
}

void runTimer(ClockType * clock) {
    while(clock->seconds != 0) {
        printClock((time_t) clock->seconds, clock);
        clock->seconds--;
        printf("\n");
        sleep(1);
    }

    return;
}

void cleanTimer(ClockType * clock) {
    cleanClock(clock);
    return;
}
