#ifndef CLOCK_H_
#define CLOCK_H_

#include <time.h>

#define CHAR_HEIGHT 9
#define CHAR_WIDTH 9

typedef struct {
    char * fmt; /* String format for the clock */
    int seconds;
} ClockType;

/* Initialize a ClockType Object */
void initClock(ClockType * clock);

/* Set the string format of a ClockType, initClock provides a default format . . . */
void setClockFmt(ClockType * clock, const char * new_fmt);

/* Print the time, using cur_time, and a ClockType */
void printClock(const time_t cur_time, const ClockType * clock);

/* Free any dynamically allocated memory used by a ClockType object */
void cleanClock(ClockType * clock);

#endif /* CLOCK_H_ */
