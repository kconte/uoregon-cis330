#include <stdio.h>
#include <stdlib.h>
#include "clock.h"

int main() {
    ClockType clock;
    
    /* Initialize clock */
    initClock(&clock);

    /* Get current timestamp */
    time_t raw;
    time(&raw);

    /* Print current time */
    printClock(raw, &clock);

    /* Free the clock's memory  */
    cleanClock(&clock);

    /* Exit cleanly */
    return EXIT_SUCCESS;
}
