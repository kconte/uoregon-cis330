#ifndef TIMER_H_
#define TIMER_H_

#include "clock.h"

/* Initialize the timer with user-provided input */
void initTimer(ClockType * clock, int minutes, int seconds);

/* Run the timer -- print out the time each second */
void runTimer(ClockType * clock);

/*  */
void cleanTimer(ClockType * clock);

#endif /* TIMER_H_ */
