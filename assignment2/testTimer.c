#include "timer.h"
#include <stdio.h>
#include <stdlib.h>

int main() {

    ClockType clock;

    int min = 0, sec = 0, valid = 0, gc = 0;

    printf("How many minutes? ");
    valid = scanf("%02d", &min);
    while(!valid) {
        while((gc = getchar()) == '\n' && gc == EOF);

        printf("How many minutes? ");
        valid = scanf("%02d", &min);
    }

    printf("How many seconds? ");
    valid = scanf("%02d", &sec);
    while(!valid) {
        while((gc = getchar()) == '\n' && gc == EOF);

        printf("How many seconds? ");
        valid = scanf("%02d", &sec);
    }

    initTimer(&clock, min, sec);

    runTimer(&clock);

    cleanTimer(&clock);

    return 0;
}
