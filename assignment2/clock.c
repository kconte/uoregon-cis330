#include "clock.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

/* ASCII Art Character Definitions */
/* It is one long string to keep the memory footprint smaller, using indexing arithmetic later on to find proper character and print */
const char ASCII[] = " ####### #      ###     # ##    #  ##   #   ##  #    ## #     ###      # #######     #       ##      # #     #  #        #        #        #        #     #######  #######         #        #        # ####### #        #        #         #######  #######         #        #        # #######         #        #        # ####### #       ##       ##       ##       ##########        #        #        #        # ####### #        #        #         #######         #        #        # #######  ####### #        #        #         ####### #       ##       ##       # ####### #########        #        #        #    #####        #        #        #        # ####### #       ##       ##       # ####### #       ##       ##       # #######  ####### #       ##       ##       # #######         #        #        # #######     #       # #     #   #   #     # ##########       ##       ##       ##       # ####### #       ##       ##       ######### #        #        #        #        #       ###     ### #   # ##  # #  ##   #   ##       ##       ##       ##       #           #####    #####    #####             #####    #####    #####           ";


/* Initialize the ClockType with the default clock format: %I:%M:%S */
/* %I: Hour in 12hr format */
/* %M: Minute */
/* %S: Second */
void initClock(ClockType * clock) {

    const char defaultFmt[] = "%I:%M:%S";
    clock->fmt = (char *) malloc(strlen(defaultFmt) + 1);

    /* DEFAULT string format for clock */
    strcpy(clock->fmt, defaultFmt);
    
    clock->seconds = 0;
    return;
}

/* Set the clock format to a different string */
void setClockFmt(ClockType * clock, const char * new_fmt) {
    free(clock->fmt);
    clock->fmt = (char *) malloc(strlen(new_fmt) + 1);
    strcpy(clock->fmt, new_fmt);
}

/* Print the current time, using cur_time as the timestamp and the clock->fmt as the string format */
void printClock(const time_t cur_time, const ClockType * clock) {

    /* If clock has not been initialized, alert user and return */
    if(clock->fmt == NULL) {
        fprintf(stderr, "ClockType not initialized!\n");
        return;
    }

    /* Allocating a buffer large enough to hold a string representation of time */
    char * buffer = (char *) malloc(128);
 
    /* Convert cur_time to a string representation, using clock->fmt as the string format */
    strftime(buffer, 128, clock->fmt, localtime(&cur_time));

    /* Get length of string representation */
    int len = strlen(buffer);

    /* Loop over each row of the ASCII characters, defined to be 9 */
    for(int i = 0; i < CHAR_HEIGHT; i++) {
        /* Loop each character in the string, for each row */
        for(int j = 0; j < len; j++) {
            /* Select proper character to print */
            /* Equation to find proper character and row to print: */
            /*      ASCII + ((CHAR_HEIGHT * CHAR_WIDTH) * [char_index]) + (i * CHAR_HEIGHT)  */
            /* + ASCII represents the location of the first character in the string (the starting place for the equation) */
            /* + CHAR_HEIGHT * CHAR_WIDTH allows us to skip over the correct amount of characters, since the characters are */
            /*   encoded by row, the multiplication gives us the total number of printable characters taken up by the ASCII*/
            /*   representation */
            /* + [char_index] is the index of ASCII in which the printable character is encoded. '0':0 '1':1 ... 'A':10, 'P':11 ... */
            /* + Finally, i * CHAR_HEIGHT gives us the correct row of the ASCII character to print. */
            switch(buffer[j]) {
                /* Digits are encoded as the first 10 characters, to make finding them easier */
                case '0': case '1': case '2': case '3': case '4':
                case '5': case '6': case '7': case '8': case '9':
                    printf("%.9s  ", ASCII + (CHAR_HEIGHT * CHAR_WIDTH * (buffer[j] - '0')) + (i * CHAR_HEIGHT));
                    break;

                /* The rest is pretty self-explanatory, each character corresponds to an index */
                /* The case statements just use the correct index . . . */
                case 'a': case 'A':
                    printf("%.9s  ", ASCII + (CHAR_HEIGHT * CHAR_WIDTH * 10) + (i * CHAR_HEIGHT));
                    break;
                case 'p': case 'P':
                    printf("%.9s  ", ASCII + (CHAR_HEIGHT * CHAR_WIDTH * 11) + (i * CHAR_HEIGHT));
                    break;
                case 'm': case 'M':
                    printf("%.9s  ", ASCII + (CHAR_HEIGHT * CHAR_WIDTH * 12) + (i * CHAR_HEIGHT));
                    break;
                case ':':
                    printf("%.9s  ", ASCII + (CHAR_HEIGHT * CHAR_WIDTH * 13) + (i * CHAR_HEIGHT));
                    break;
                /* Since the ' ' character is just a bunch of spaces (and is of a different width), it is not encoded in ASCII */
                case ' ':
                    printf("      ");
                /* If a character is encountered that is not encoded, it is skipped from the printing */
                default: break;
            }
        }
        /* Print a new line after every row */
        printf("\n");
    }

    /* Free memory used by the buffer containing the string representation of cur_time */
    free(buffer);

    return;
}

/* Deallocate any dynamically allocated memory used by a ClockType object */
void cleanClock(ClockType * clock) {
    free(clock->fmt);
    return;
}
