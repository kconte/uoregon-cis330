Played catch up. The snow storm earlier this week really hindered my ability to work.

Finalized settings.
All settings can now be edited and saved by the user.
    + Sales Tax %
    + Company Information
        - Name
        - Phone
        - Email
        - Address
    + File Paths
        - Inventory
        - Open Tabs
        - Application Logs
        - Sales Logs
        - Receipts
Settings edits now include input validation as well.

Began work on Inventory Management
    + CSV File I/O
        - Load inventory into memory as a bunch of Item objects in a vector
        - Save changes to file after a transaction is completed
