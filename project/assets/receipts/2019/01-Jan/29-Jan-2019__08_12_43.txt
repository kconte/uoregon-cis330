29 Jan 2019  08:12:43
=====================
   2 x Wheat Bread - Sliced @ $4.00 ea. = $8.00
   5 x Cake Donut @ $1.50 ea. = $7.50
   2 x Coffee - Decaffeinated @ $1.00 ea. = $2.00
   4 x White Bread @ $2.25 ea. = $9.00
   3 x Coffee @ $1.00 ea. = $3.00
   2 x Sourdough Bread - Sliced @ $4.50 ea. = $9.00
   3 x Orange Juice - Bottled @ $2.50 ea. = $7.50
   5 x Hot Tea - Decaffeinated @ $1.25 ea. = $6.25
   1 x Pumpkin Pie - 10 in @ $12.00 ea. = $12.00
   5 x Apple Pie - Slice @ $3.00 ea. = $15.00

Subtotal: $79.25

Tax(10.75%): $8.52
Total: $87.77

Cash Tendered: $90.00
Change Due: $2.23
