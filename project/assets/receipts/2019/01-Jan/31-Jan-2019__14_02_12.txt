31 Jan 2019  14:02:12
=====================
   2 x Pumpkin Pie - 10 in @ $12.00 ea. = $24.00
   3 x Apple Pie - Slice @ $3.00 ea. = $9.00
   5 x Orange Juice - Bottled @ $2.50 ea. = $12.50
   2 x Hot Tea - Decaffeinated @ $1.25 ea. = $2.50
   2 x French Baguette @ $4.00 ea. = $8.00
   3 x Orange Juice - Fresh @ $4.00 ea. = $12.00
   2 x White Bread @ $2.25 ea. = $4.50
   2 x Apple Pie - 10 in @ $12.00 ea. = $24.00
   4 x Pumpkin Pie - Slice @ $3.00 ea. = $12.00

Subtotal: $108.50

Tax(10.75%): $11.66
Total: $120.16

Cash Tendered: $130.00
Change Due: $9.84
