 1 Jan 2019  13:32:57
=====================
   3 x Sourdough Bread @ $3.50 ea. = $10.50
   3 x Maple Bar @ $2.50 ea. = $7.50
   5 x Apple Pie - Slice @ $3.00 ea. = $15.00
   3 x Pumpkin Pie - 10 in @ $12.00 ea. = $36.00
   3 x Chocolate Glazed Donut @ $1.50 ea. = $4.50
   5 x Naan Bread @ $1.25 ea. = $6.25
   5 x Wheat Bread @ $3.00 ea. = $15.00

Subtotal: $94.75

Tax(10.75%): $10.19
Total: $104.94

Cash Tendered: $110.00
Change Due: $5.06
