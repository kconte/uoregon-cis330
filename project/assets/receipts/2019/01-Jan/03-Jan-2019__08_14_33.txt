 3 Jan 2019  08:14:33
=====================
   3 x Bottled Soda @ $1.00 ea. = $3.00
   1 x Maple Bar @ $2.50 ea. = $2.50
   4 x White Bread - Sliced @ $3.50 ea. = $14.00
   4 x Bottled Water @ $0.75 ea. = $3.00
   3 x Bear Claw @ $2.50 ea. = $7.50
   1 x Wheat Bread @ $3.00 ea. = $3.00
   5 x Pumpkin Pie - Slice @ $3.00 ea. = $15.00
   3 x French Baguette @ $4.00 ea. = $12.00
   2 x Chocolate Glazed Donut @ $1.50 ea. = $3.00

Subtotal: $63.00

Tax(10.75%): $6.77
Total: $69.77

Cash Tendered: $80.00
Change Due: $10.23
