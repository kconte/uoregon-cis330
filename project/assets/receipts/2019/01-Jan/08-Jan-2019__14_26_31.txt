 8 Jan 2019  14:26:31
=====================
   3 x Coffee @ $1.00 ea. = $3.00
   2 x Sourdough Bread @ $3.50 ea. = $7.00
   3 x Bottled Soda @ $1.00 ea. = $3.00
   5 x Cake Donut @ $1.50 ea. = $7.50
   2 x Hot Tea - Decaffeinated @ $1.25 ea. = $2.50
   1 x White Bread - Sliced @ $3.50 ea. = $3.50
   1 x Sourdough Bread - Sliced @ $4.50 ea. = $4.50
   2 x Naan Bread @ $1.25 ea. = $2.50

Subtotal: $33.50

Tax(10.75%): $3.60
Total: $37.10

Cash Tendered: $40.00
Change Due: $2.90
