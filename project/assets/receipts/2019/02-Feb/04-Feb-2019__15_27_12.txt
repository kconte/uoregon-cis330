 4 Feb 2019  15:27:12
=====================
   1 x Apple Pie - Slice @ $3.00 ea. = $3.00
   4 x Hot Tea @ $1.25 ea. = $5.00
   3 x Sourdough Bread @ $3.50 ea. = $10.50
   4 x White Bread - Sliced @ $3.50 ea. = $14.00

Subtotal: $32.50

Tax(10.75%): $3.49
Total: $35.99

Cash Tendered: $40.00
Change Due: $4.01
