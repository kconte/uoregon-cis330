24 Feb 2019  11:17:17
=====================
   1 x Sourdough Bread - Sliced @ $4.50 ea. = $4.50
   5 x White Bread - Sliced @ $3.50 ea. = $17.50
   4 x French Baguette @ $4.00 ea. = $16.00
   4 x Wheat Bread - Sliced @ $4.00 ea. = $16.00

Subtotal: $54.00

Tax(10.75%): $5.80
Total: $59.80

Cash Tendered: $70.00
Change Due: $10.20
