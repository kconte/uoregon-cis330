11 Feb 2019  13:14:09
=====================
   1 x Maple Bar @ $2.50 ea. = $2.50
   1 x Apple Pie - 10 in @ $12.00 ea. = $12.00
   4 x Bottled Soda @ $1.00 ea. = $4.00
   1 x French Baguette @ $4.00 ea. = $4.00
   4 x Pumpkin Pie - 10 in @ $12.00 ea. = $48.00
   3 x Sourdough Bread - Sliced @ $4.50 ea. = $13.50
   2 x Coffee @ $1.00 ea. = $2.00
   5 x Bear Claw @ $2.50 ea. = $12.50
   1 x Pumpkin Pie - 12 in @ $14.00 ea. = $14.00
   3 x Wheat Bread - Sliced @ $4.00 ea. = $12.00

Subtotal: $124.50

Tax(10.75%): $13.38
Total: $137.88

Cash Tendered: $140.00
Change Due: $2.12
