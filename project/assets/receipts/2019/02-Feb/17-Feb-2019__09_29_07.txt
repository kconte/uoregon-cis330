17 Feb 2019  09:29:07
=====================
   4 x Glazed Donut @ $1.50 ea. = $6.00
   3 x Maple Bar @ $2.50 ea. = $7.50
   5 x Cake Donut @ $1.50 ea. = $7.50

Subtotal: $21.00

Tax(10.75%): $2.26
Total: $23.26

Cash Tendered: $30.00
Change Due: $6.74
