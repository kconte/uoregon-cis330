17 Feb 2019  08:34:02
=====================
   5 x White Bread @ $2.25 ea. = $11.25
   4 x Apple Pie - 10 in @ $12.00 ea. = $48.00
   2 x White Bread - Sliced @ $3.50 ea. = $7.00

Subtotal: $66.25

Tax(10.75%): $7.12
Total: $73.37

Cash Tendered: $80.00
Change Due: $6.63
