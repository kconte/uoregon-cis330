16 Feb 2019  09:17:50
=====================
   5 x Orange Juice - Fresh @ $4.00 ea. = $20.00
   1 x Chocolate Glazed Donut @ $1.50 ea. = $1.50
   5 x Apple Pie - 10 in @ $12.00 ea. = $60.00
   4 x White Bread @ $2.25 ea. = $9.00

Subtotal: $90.50

Tax(10.75%): $9.73
Total: $100.23

Cash Tendered: $110.00
Change Due: $9.77
