 6 Feb 2019  13:23:17
=====================
   2 x Apple Pie - 12 in @ $14.00 ea. = $28.00
   5 x Sourdough Bread - Sliced @ $4.50 ea. = $22.50
   2 x Pumpkin Pie - 12 in @ $14.00 ea. = $28.00
   3 x Hot Tea - Decaffeinated @ $1.25 ea. = $3.75
   2 x Wheat Bread @ $3.00 ea. = $6.00
   4 x Glazed Donut Hole @ $0.25 ea. = $1.00
   4 x Bottled Water @ $0.75 ea. = $3.00
   5 x Cake Donut @ $1.50 ea. = $7.50

Subtotal: $99.75

Tax(10.75%): $10.72
Total: $110.47

Cash Tendered: $120.00
Change Due: $9.53
