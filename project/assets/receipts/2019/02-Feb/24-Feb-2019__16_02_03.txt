24 Feb 2019  16:02:03
=====================
   2 x Coffee - Decaffeinated @ $1.00 ea. = $2.00
   1 x White Bread - Sliced @ $3.50 ea. = $3.50
   1 x Coffee @ $1.00 ea. = $1.00
   1 x Bottled Water @ $0.75 ea. = $0.75

Subtotal: $7.25

Tax(10.75%): $0.78
Total: $8.03

Cash Tendered: $10.00
Change Due: $1.97
