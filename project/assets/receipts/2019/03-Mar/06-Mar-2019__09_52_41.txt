 6 Mar 2019  09:52:41
=====================
   5 x Coffee @ $1.00 ea. = $5.00
   1 x Coffee - Decaffeinated @ $1.00 ea. = $1.00
   3 x Sourdough Bread - Sliced @ $4.50 ea. = $13.50
   3 x Hot Tea - Decaffeinated @ $1.25 ea. = $3.75
   5 x Maple Bar @ $2.50 ea. = $12.50
   2 x White Bread - Sliced @ $3.50 ea. = $7.00
   1 x French Baguette @ $4.00 ea. = $4.00
   2 x Bear Claw @ $2.50 ea. = $5.00
   1 x Apple Pie - 12 in @ $14.00 ea. = $14.00

Subtotal: $65.75

Tax(10.75%): $7.07
Total: $72.82

Cash Tendered: $80.00
Change Due: $7.18
