 8 Mar 2019  12:37:39
=====================
   2 x Hot Tea @ $1.25 ea. = $2.50
   1 x Coffee @ $1.00 ea. = $1.00
   2 x White Bread @ $2.25 ea. = $4.50
   2 x Orange Juice - Fresh @ $4.00 ea. = $8.00
   4 x White Bread - Sliced @ $3.50 ea. = $14.00
   4 x Chocolate Glazed Donut @ $1.50 ea. = $6.00
   5 x Maple Bar @ $2.50 ea. = $12.50

Subtotal: $48.50

Tax(10.75%): $5.21
Total: $53.71

Cash Tendered: $60.00
Change Due: $6.29
