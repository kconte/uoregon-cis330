11 Mar 2019  09:17:51
=====================
   2 x Naan Bread @ $1.25 ea. = $2.50
   5 x Apple Pie - Slice @ $3.00 ea. = $15.00
   2 x Orange Juice - Fresh @ $4.00 ea. = $8.00
   3 x Glazed Donut @ $1.50 ea. = $4.50
   5 x French Baguette @ $4.00 ea. = $20.00
   5 x Coffee - Decaffeinated @ $1.00 ea. = $5.00

Subtotal: $55.00

Tax(10.75%): $5.91
Total: $60.91

Cash Tendered: $70.00
Change Due: $9.09
