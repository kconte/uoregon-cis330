12 Mar 2019  12:29:03
=====================
   1 x Chocolate Glazed Donut @ $1.50 ea. = $1.50
   4 x Apple Pie - Slice @ $3.00 ea. = $12.00
   4 x Bottled Water @ $0.75 ea. = $3.00
   1 x Apple Pie - 12 in @ $14.00 ea. = $14.00
   3 x Pumpkin Pie - 10 in @ $12.00 ea. = $36.00
   5 x Glazed Donut @ $1.50 ea. = $7.50
   3 x White Bread @ $2.25 ea. = $6.75
   3 x Wheat Bread @ $3.00 ea. = $9.00

Subtotal: $89.75

Tax(10.75%): $9.65
Total: $99.40

Cash Tendered: $110.00
Change Due: $10.60
