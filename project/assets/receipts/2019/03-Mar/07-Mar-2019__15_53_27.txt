 7 Mar 2019  15:53:27
=====================
   2 x White Bread - Sliced @ $3.50 ea. = $7.00
   1 x Hot Tea - Decaffeinated @ $1.25 ea. = $1.25
   1 x Cake Donut @ $1.50 ea. = $1.50
   2 x Wheat Bread @ $3.00 ea. = $6.00

Subtotal: $15.75

Tax(10.75%): $1.69
Total: $17.44

Cash Tendered: $20.00
Change Due: $2.56
