14 Mar 2019  14:19:43
=====================
   5 x Hot Tea @ $1.25 ea. = $6.25
   3 x Pumpkin Pie - Slice @ $3.00 ea. = $9.00
   4 x Bottled Water @ $0.75 ea. = $3.00
   3 x Hot Tea - Decaffeinated @ $1.25 ea. = $3.75

Subtotal: $22.00

Tax(10.75%): $2.36
Total: $24.36

Cash Tendered: $30.00
Change Due: $5.64
