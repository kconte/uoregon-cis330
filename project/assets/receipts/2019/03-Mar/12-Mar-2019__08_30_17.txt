12 Mar 2019  08:30:17
=====================
   3 x Sourdough Bread @ $3.50 ea. = $10.50
   4 x Orange Juice - Fresh @ $4.00 ea. = $16.00
   2 x Sourdough Bread - Sliced @ $4.50 ea. = $9.00
   1 x Cake Donut @ $1.50 ea. = $1.50
   3 x Pumpkin Pie - Slice @ $3.00 ea. = $9.00
   5 x Bear Claw @ $2.50 ea. = $12.50
   5 x Coffee @ $1.00 ea. = $5.00
   3 x White Bread - Sliced @ $3.50 ea. = $10.50
   2 x Coffee - Decaffeinated @ $1.00 ea. = $2.00
   2 x White Bread @ $2.25 ea. = $4.50

Subtotal: $80.50

Tax(10.75%): $8.65
Total: $89.15

Cash Tendered: $100.00
Change Due: $10.85
