 5 Mar 2019  15:39:40
=====================
   1 x Hot Tea - Decaffeinated @ $1.25 ea. = $1.25
   2 x Cake Donut @ $1.50 ea. = $3.00
   1 x Bear Claw @ $2.50 ea. = $2.50
   1 x White Bread - Sliced @ $3.50 ea. = $3.50

Subtotal: $10.25

Tax(10.75%): $1.10
Total: $11.35

Cash Tendered: $20.00
Change Due: $8.65
