#!/bin/python

from random import randint, uniform
from math import ceil
import re
import os

class Item:
    def __init__(self, name, price, quantity):
        self.name = name
        self.price = price
        self.quantity = quantity
        return

def isLeap(year):
    result = False
    if year % 4 == 0: result = True
    if year % 100 == 0: result = False
    if year % 400 == 0: result = True
    return result

def generateRandomCash(total):
    cash = int(ceil(total))
    cash += (10 - (cash % 10))
    return float(cash)

def generateReceipt(items):
    global months

    bill = []
    numItems = randint(1, 10)
    indeces = set()
    for i in range(numItems):
        index = 0
        while(True):
            index = randint(0, len(items) - 1)
            if index not in indeces:
                indeces.add(index)
                break
        quantity = randint(1, 5)
        bill.append(Item(items[index].name, items[index].price, quantity))

    subtotal = sum(item.price * item.quantity for item in bill)
    tax = subtotal * 0.1075
    total = subtotal + tax

    cash = generateRandomCash(total)
    change = cash - total

    year = randint(2019, 2019)
    month = randint(1, 3)
    day = randint(1, 31)
    if month == 2: day = randint(1, 28)
    if month == 2 and isLeap(year): day = randint(1, 29)
    if month == 3: day = randint(1, 15)
    if month in [4, 6, 9, 11]: day = randint(1, 30)

    hour = randint(8, 17)
    minute = randint(0, 59)
    second = randint(0, 59)

    receiptfilename = './receipts/{2}/{6:02}-{1}/{0:02}-{1}-{2}__{3:02}_{4:02}_{5:02}.txt'.format(day, months[month], year, hour, minute, second, month)

    if not os.path.exists(os.path.dirname(receiptfilename)):
        os.makedirs(os.path.dirname(receiptfilename))

    with open(receiptfilename, 'w+') as f:
        f.write('{0:2} {1} {2}  {3:02}:{4:02}:{5:02}\n'.format(day, months[month], year, hour, minute, second))
        f.write('=====================\n')
        for item in bill:
            f.write('{0:4} x {1} @ ${2:.2f} ea. = ${3:.2f}\n'.format(item.quantity, item.name, item.price, item.price * item.quantity))
        f.write('\nSubtotal: ${0:.2f}\n\n'.format(subtotal))
        f.write('Tax(10.75%): ${0:.2f}\n'.format(tax))
        f.write('Total: ${0:.2f}\n\n'.format(total))
        f.write('Cash Tendered: ${0:.2f}\n'.format(cash))
        f.write('Change Due: ${0:.2f}\n'.format(change))

    saleslogfilename = './sales-logs/{2}/{3:02}-{1}/{0:02}'.format(day, months[month], year, month)
    if not os.path.exists(os.path.dirname(saleslogfilename)):
        os.makedirs(os.path.dirname(saleslogfilename))
    with open(saleslogfilename, 'a+') as f:
        f.write('"{0:02}:{1:02}","{2:.2f}","{3:.2f}","{4:.2f}"\n'.format(hour, minute, subtotal, tax, total))


items = []

months = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

with open('inventory.csv', 'r') as f:
    for line in f:
        pieces = re.findall(r'"([^"]*)"', line)
        items.append(Item(pieces[0], float(pieces[1]), int(pieces[2])))

for i in range(300):
    print(i, end='\r')
    generateReceipt(items)
