#include "engine.hpp"
#include "item.hpp"

#include <algorithm>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <thread>

using namespace std;

void Engine::loadInventory() {
    
    /* stream to read the inventory file */
    ifstream inv;

    /* Open the file */
    inv.open(this->inventoryFilePath);

    /* The file will be open if it was found */
    if(inv.is_open()) {

        string line;

        /* Read each line into an Item object and add to the inventory vector */
        while(getline(inv, line)) {

            /* next item object */
            Item next;

            /* Grab name of item (first col in csv) */
            auto start = line.find('"');
            auto end = line.find('"', start + 1);
            string name = line.substr(start + 1, end - 1);
            next.setName(name);

            /* Grab price per unit of item (second col in csv) */
            start = line.find('"', end + 1);
            end = line.find('"', start + 1);
            float price = stof(line.substr(start + 1, end - 1));
            next.setPrice(price);

            /* Grab quantity in stock of item (third col in csv) */
            start = line.find('"', end + 1);
            end = line.find('"', start + 1);
            int quantity = stoi(line.substr(start + 1, end - 1));
            next.setQuantity(quantity);

            /* Add item to inventory vector */
            this->inventory.push_back(next);
        }
    } else {
        /* If the file is not open, it couldn't find the file, exit with code -1 */
        cerr << "Could not open " << this->inventoryFilePath << " for reading!" << endl;
        exit(-1);
    }

    /* Close the file */
    inv.close();

    /* Sort the inventory based on item name */
    sort(this->inventory.begin(), this->inventory.end());
}

/* Saves the current memory state of the inventory to file */
void Engine::saveInventory() {
    ofstream out;
    out.open(this->inventoryFilePath);

    if(out.is_open()) {
        /* Print each item into the inventory file */
        for (auto it = this->inventory.begin(); it != this->inventory.end(); ++it) {
            out << "\"" << (*it).getName() << "\",\""
                << fixed << setprecision(2) << (*it).getPrice()
                << "\",\"" << (*it).getQuantity() << "\"" << endl;
        }

    } else {
        cerr << "Could not open " << this->inventoryFilePath << " for writing!" << endl;
        exit(-1);
    }

    /* Close the file to avoid memory leaks */
    out.close();
}

/* Main Inventory Menu Loop */
void Engine::inventoryLoop() {
    
    /* Loop until valid input */
    bool keepGoing = true;
    string line;
    while(keepGoing == true) {
        /* Clear the screen for a cleaner UI */
        this->clearScreen();

        /* Print the main inventory menu */
        this->printInventoryMenu();

        /* Get input from user */
        int choice = this->getInt();

        /* Select appropriate action based on input */
        switch(choice) {
            /* Display */
            case 1:
                /* Clear screen for a cleaner UI */
                this->clearScreen();
                cout << "Inventory" << endl;
                cout << "=========" << endl;
                /* Print the conetnets of the inventory */
                this->displayInventory();
                cout << endl << "Press <Enter> to continue . . .";
                getline(cin, line);
                break;

            /* Add an item to the inventory */
            case 2:
                this->addItemToInventory();
                break;

            /* Remove an item from the inventory */
            case 3:
                this->removeItemFromInventory();
                break;

            /* Edit an item in the inventory */
            case 4:
                this->editItemInInventory();
                break;

            /* Go Back */
            case 5:
                keepGoing = false;
            default:
                break;
        }
    }

    /* Save inventory to file */
    this->saveInventory();
}

/* Prints the options available for inventory management */
void Engine::printInventoryMenu() {
    cout << "Inventory" << endl;
    cout << "=========" << endl;
    cout << "[1] Display Items" << endl;
    cout << "[2] Add Item" << endl;
    cout << "[3] Remove Item" << endl;
    cout << "[4] Edit Item" << endl;
    cout << "[5] Go Back" << endl;

    this->printConfirmMessage();
}

/* Print the inventory to the screen */
void Engine::displayInventory() {
    int i = 1;
    for(auto it = this->inventory.begin(); it != this->inventory.end(); ++it) {
        cout << "[" << setw(2) << setfill(' ') << i++ << "]  " << *it << endl;
    }
}

/* Adds an item to the inventory, taking the user through a guided screen */
void Engine::addItemToInventory() {
    /* Clear the screen for a cleaner UI */
    this->clearScreen();

    string name;
    float price;
    int quantity;

    /* Get the name of the new item from the user, exiting if blank input */
    cout << "Item name (blank for cancel): ";
    getline(cin, name);
    if(name == "") {
        cout << "No changes made." << endl;
        this_thread::sleep_for(1s);
        return;
    }

    /* Get the price per unit of the item, exiting if blank input */
    cout << "Price per unit (blank for cancel): $";
    price = this->getFloat();
    if(price == -1.0) {
        cout << "No changes made." << endl;
        this_thread::sleep_for(1s);
        return;
    }

    /* Get the quantity of the new item, exiting if blank input */
    cout << "Quantity in Stock (blank for cancel): ";
    quantity = this->getInt();
    if(quantity == -1) {
        cout << "No changes made." << endl;
        this_thread::sleep_for(1s);
        return;
    }

    /* Create the new item */
    Item newItem{name, price, quantity};

    /* Confirm the creation of the new item */
    cout << endl << newItem << endl;
    cout << "Confirm [Y/n]?" << endl;
    string line;
    getline(cin, line);
    if(line == "" || line == "y" || line == "Y") {
        cout << endl << "Checking if item exists already . . ." << endl;
    } else {
        cout << "No changes made." << endl;
        this_thread::sleep_for(1s);
        return;
    }

    /* If the item is already in the inventory, don't add it */
    if(this->itemIn(name) != this->inventory.end()) {
        cout << "Item already exists!" << endl;
        this_thread::sleep_for(1s);
    } else {
        /* Otherwise add it to the inventory */
        this->inventory.push_back(newItem);

        /* Keep the inventory sorted */
        sort(this->inventory.begin(), this->inventory.end());
    }
}

/* Remove and item from the inventory */
void Engine::removeItemFromInventory() {
    bool validItem = false;
    string line;
    vector<Item>::iterator it;

    /* Loop until a valid item is selected */
    while(!validItem) {
        /* Clear the screen for a cleaner UI */
        this->clearScreen();

        /* Print the list of items to the screen */
        cout << "Remove Item" << endl;
        cout << "===========" << endl;
        this->displayInventory();
        cout << endl << "[0] Cancel" << endl;
        this->printConfirmMessage();

        /* Bounds check the input, if out of bounds, then ask again */
        /* If choice equals 0 (Cancel), then make no changes and exit */
        /* Otherwise, a valid item has selected, and this loop can exit */
        int choice = this->getInt();
        if(choice < 0 || choice > static_cast<int>(this->inventory.size())) {
            continue;
        } else if(choice == 0) {
            cout << "No changes made." << endl;
            this_thread::sleep_for(1s);
            return;
        }
        else {
            it = this->inventory.begin() + choice - 1;
            validItem = true;
        }
    }

    /* Confirm deletion of selected item */
    cout << endl <<  *it << endl << endl;
    cout << "Deleting: " << (*it).getName() << endl;
    cout << "Confirm [Y/n]?" << endl;
    getline(cin, line);
    if(line == "" || line == "y" || line == "Y") {
        cout << (*it).getName() << " deleted." << endl;
        Item toDel{(*it).getName(), (*it).getPrice(), (*it).getQuantity()};
        this->inventory.erase(remove(this->inventory.begin(), this->inventory.end(), toDel), this->inventory.end());
    } else {
        cout << "Changes not saved." << endl;
        this_thread::sleep_for(1s);
    }
}

/* Edits an item in the inventory, taking the user through a guided screen */
void Engine::editItemInInventory() {

    bool validItem = false;
    string line;
    vector<Item>::iterator it;

    /* Loop until a valid item has been selected */
    while(!validItem) {

        /* Clear the screen for a cleaner UI */
        this->clearScreen();

        /* Print the item list to the screen */
        cout << "Edit Item" << endl;
        cout << "=========" << endl;
        this->displayInventory();
        cout << endl << "[0] Cancel" << endl;
        this->printConfirmMessage();
        int choice = this->getInt();

        /* Bounds check the selection, if it is out of bounds, ask again */
        /* If choice equals 0 (Cancel) return having made no changes */
        /* Otherwise, a valid item has been selected and this loop can exit */
        if(choice < 0 || choice > static_cast<int>(this->inventory.size()) + 1) {
            continue;
        } else if(choice == 0) {
            cout << "No changes made." << endl;
            this_thread::sleep_for(1s);
            return;
        } else {
            it = this->inventory.begin() + choice - 1;
            validItem = true;
        }
    }

    /* Print the item to be edited */
    line = "Edit " + (*it).getName();
    string name = (*it).getName();
    float price = (*it).getPrice();
    int quantity = (*it).getQuantity();

    /* Keep looping until a valid selection is made */
    bool keepGoing = true;
    while(keepGoing == true) {
        /* Clear the screen for a cleaner UI */
        this->clearScreen();

        /* Print menu options */
        cout << line << endl;
        cout << setw(line.length()) << setfill('=') << "" << endl;
        cout << "[1] Name (" << name << ")" << endl;
        cout << "[2] Price ($" << fixed << setprecision(2) << price << ")" << endl;
        cout << "[3] Quantity (" << quantity << ")" << endl;
        cout << "[4] Save and Go Back" << endl;

        this->printConfirmMessage();

        /* Get input from user */
        int choice = this->getInt();

        /* Select appropriate option based on input */
        switch(choice) {
            /* edit name */
            case 1:
                this->editValue(name, 1);
                break;

            /* edit price */
            case 2:
                this->editPrice(price);
                break;

            /* edit quantity */
            case 3:
                this->editQuantity(quantity);
                break;

            /* cancel */
            case 4:
                keepGoing = false;
            default:
                break;
        }
    }

    /* Update the item's record with the new values */
    (*it).setName(name);
    (*it).setPrice(price);
    (*it).setQuantity(quantity);
}

/* Edits the price of an item */
void Engine::editPrice(float & val) {
    /* Clear the screen for a cleaner UI */
    this->clearScreen();

    /* Print the current value and request a new value */
    cout << "Current value: $" << fixed << setprecision(2) << val << endl << endl;
    cout << "Enter new value (blank for cancel): $";
    float newVal = this->getFloat();

    /* If value is invalid, return having made no changes */
    if(newVal == -1.0) {
        cout << "No changes made." << endl;
        this_thread::sleep_for(1s);
        return;
    }

    /* Confirm the new value before updating */
    cout << endl << "New value is: $" << fixed << setprecision(2) << newVal << endl;
    cout << "Confirm [Y/n]?" << endl;
    string line = "";
    getline(cin, line);
    if(line == "" || line == "y" || line == "Y") {
        val = newVal;
    } else {
        cout << "Changes not saved." << endl;
        this_thread::sleep_for(1s);
    }
}

/* Edits the quantity of an item */
void Engine::editQuantity(int & val) {
    /* clear the screen for a cleaner UI */
    this->clearScreen();

    /* Print current value and request new one */
    cout << "Current value: " << val << endl << endl;
    cout << "Enter new value (blank for cancel): ";
    int newVal = this->getInt();

    /* If new value is invalid, exit having made no changes */
    if(newVal == -1) {
        cout << "No changes made." << endl;
        this_thread::sleep_for(1s);
        return;
    }

    /* Confirm new value before updating */
    cout << endl << "New value is: " << newVal << endl;
    cout << "Confirm [Y/n]?" << endl;
    string line = "";
    getline(cin, line);
    if(line == "" || line == "y" || line == "Y") {
        cout << "Changes saved." << endl;
        val = newVal;
    } else {
        cout << "Changes not saved." << endl;
        this_thread::sleep_for(1s);
    }
}

/* Determines if an item with a given name exists within the inventory */
vector<Item>::iterator Engine::itemIn(string & name) {
    for(auto it = this->inventory.begin(); it != this->inventory.end(); ++it) {
        if((*it).getName() == name)
            return it;
    }
    return this->inventory.end();
}
