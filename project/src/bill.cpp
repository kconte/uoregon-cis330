#include "bill.hpp"
#include "engine.hpp"
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <thread>

/* Default constructor */
Bill::Bill() {
    this->items = vector<Item>(0);
    this->subtotal = 0.0f;
    this->total = 0.0f;
    this->salesTax = 0.0f;
}

/* Input constructor */
Bill::Bill(float tax) : Bill() {
    this->salesTax = tax;
}

/* Print to cout by default */
void Bill::print() {
    this->print(cout);
}

/* Print the Bill to a provided stream */
void Bill::print(ostream & os) {
    /*
        Print each item
        Quantity x Name @ Price = Total
    */
    for(auto it = this->items.begin(); it != this->items.end(); ++it) {
        os << setw(4) << setfill(' ') << (*it).getQuantity() << " x " << (*it).getName() << " @ $"
             << fixed << setprecision(2) << (*it).getPrice() << " ea. = $"
             << fixed << setprecision(2) << (*it).getPrice() * (*it).getQuantity() << endl;
    }

    /* Print the subtotal for the bill (before any tax is added) */
    os << endl << "Subtotal: $" << fixed << setprecision(2) << this->subtotal << endl << endl;
}

/* Add an item to the bill, taking a vector of items as available stock */
void Bill::addItem(vector<Item> & inventory) {
    /* Select an item, if the user selects 0 (cancels), then return */
    int choice = this->selectItem(inventory);
    if(choice == 0) {
        cout << "No changes made." << endl;
        this_thread::sleep_for(1s);
        return;
    }

    /* Decrement the choice for proper indexing */
    choice--;

    /* Determine if the choice is already on the bill, if so do nothing */
    for(auto it = this->items.begin(); it != this->items.end(); ++it) {
        if(inventory.at(choice).getName() == (*it).getName()) {
            cout << "Item already in bill!" << endl;
            this_thread::sleep_for(1s);
            return;
        }
    }

    /* Loop until valid input */
    int quant = 0;
    bool keepGoing = true;
    while(keepGoing == true) {
        /* Clear screen for a cleaner UI */
        Engine::clearScreen();

        /* Request number of item to add */
        cout << inventory.at(choice).getName() << endl;
        cout << setw(inventory.at(choice).getName().length()) << setfill('=') << "" << endl;
        cout << "$" << fixed << setprecision(2) << inventory.at(choice).getPrice() << " ea." << endl;
        cout << "How many (" << inventory.at(choice).getQuantity() << " avail.) [0 for cancel]? ";
        quant = Engine::getInt();

        /* If the user enters 0, cancel and return */
        if(quant == 0) {
            cout << endl << "No changes made." << endl;
            this_thread::sleep_for(1s);
            return;
        }
        /* Otherwise, bounds check */
        else if(quant > inventory.at(choice).getQuantity() || quant < 0) {
            cout << "Please enter a valid number." << endl;
            this_thread::sleep_for(1s);
        } else {
            keepGoing = false;
        }
    }

    /* Confirm quantity of item to add and add appropriately */
    cout << "Quantity to add: " << quant << endl;
    cout << "Confirm [Y/n]? ";
    string line;
    getline(cin, line);
    if(line == "" || line == "y" || line == "Y") {
        cout << "Adding " << quant << " " << inventory.at(choice).getName() << " to bill . . ." << endl;
        Item newItem;
        newItem.setName(inventory.at(choice).getName());
        newItem.setPrice(inventory.at(choice).getPrice());
        newItem.setQuantity(quant);
        this->items.push_back(newItem);
        /* Sort the bill */
        sort(this->items.begin(), this->items.end());
    } else {
        cout << "No changes made." << endl;
    }

    /* Update the totals for the bill */
    this->updateTotals();
}

/* Remove an item from the bill */
void Bill::removeItem() {
    /* If the bill is already empty, then no reason to try to remove anything */
    if(this->items.size() == 0) {
        cout << "No items to remove!" << endl;
        this_thread::sleep_for(1s);
        return;
    }

    /* Select an item from the bill, if user selects 0 (Cancel), then return having made no changes */
    int choice = this->selectItem();
    if(choice == 0) {
        cout << "No changes made." << endl;
        this_thread::sleep_for(1s);
        return;
    }

    /* Decrement choice for proper indexing */
    choice--;

    /* Confirm selection to delete and delete appropriately */
    cout << endl << "Deleting: " << this->items.at(choice).getName() << endl;
    cout << "Confirm [Y/n]? ";
    string line;
    getline(cin, line);
    if(line == "" || line == "y" || line == "Y") {
        cout << "Item Deleted." << endl;
        Item toDel{this->items.at(choice).getName(), this->items.at(choice).getPrice(), this->items.at(choice).getQuantity()};
        this->items.erase(remove(this->items.begin(), this->items.end(), toDel), this->items.end());
    } else {
        cout << "No changes made." << endl;
    }

    /* Update the totals of the bill */
    this->updateTotals();
    this_thread::sleep_for(1s);
}

/* By default, select from the bill's own items */
int Bill::selectItem() {
    return this->selectItem(this->items);
}

/* Select an item from a vector of Item objects, and return the selected index */
int Bill::selectItem(vector<Item> & items) {
    int choice = 0;

    /* Loop until valid input */
    bool keepGoing = true;
    while(keepGoing == true) {

        /* Clear the screen for a cleaner UI */
        Engine::clearScreen();

        /* Print each item for selection */
        int i = 1;
        for(auto it = items.begin(); it != items.end(); ++it) {
            cout << "[" << setw(2) << setfill(' ') << i++ << "] "
                << *it << endl;
        }
        cout << endl << "[0] Cancel" << endl;
        Engine::printConfirmMessage();

        /* Retrieve int from user for choice */
        choice = Engine::getInt();

        /* Bounds check, if choice out of bounds, keep looping */
        if(choice < i && choice >= 0) {
            keepGoing = false;
        }
    }
    return choice;
}

/* Update the totals withing the bill */
void Bill::updateTotals() {
    /* For each item, add to the subtotal the item's price times the item's quantity */
    this->subtotal = accumulate(this->items.begin(),
            this->items.end(),
            0.0, 
            [] (double & val, Item & item) { return val + (item.getPrice() * item.getQuantity()); });

    /* To get the total, multiply the subtotal times the sales tax percentage plus 1 */
    this->total = this->subtotal * (1.0 + this->salesTax);
}
