#include "bill.hpp"
#include "engine.hpp"
#include "item.hpp"

#include <algorithm>
#include <chrono>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <thread>
#include <vector>
#include <time.h>
#include <unistd.h>

#define PST -7

/* Main loop of the transaction menu */
void Engine::transaction() {
    /* Create a new bill, to tally items and amount due */
    Bill bill{this->taxPercentage / 100};
    string line;

    /* Flag to keep looping */
    bool keepGoing = true;
    while(keepGoing == true) {

        /* Clear screen for a cleaner UI */
        this->clearScreen();

        /* Print the menu */
        this->printTransactionMenu();

        /* Print current subtotal of bill */
        cout << endl << "Subtotal: $" << fixed << setprecision(2) << bill.getSubtotal() << endl;

        /* Print confirm message, tell the user how to confirm input */
        this->printConfirmMessage();

        /* Get input from the user */
        int choice = this->getInt();

        /* Flag used to determine if the checkout was successful */
        bool success = false;

        switch(choice) {
            /* View the bill */
            case 1:
                /* If the bill is empty, no need to display an empty bill */
                if(bill.getItems().size() == 0) {
                    cout << "No items on bill!" << endl;
                    this_thread::sleep_for(1s);
                } else {
                    /* Clear the screen for a cleaner UI */
                    this->clearScreen();

                    /* Print the bill */
                    bill.print();
                    cout << "Press <Enter> to continue . . .";
                    getline(cin, line);
                }
                break;
            case 2:
                /* Add an item to the bill */
                bill.addItem(this->inventory);
                break;
            case 3:
                /* Remove an item from the bill */
                bill.removeItem();
                break;
            case 4:
                /* Checkout */
                success = this->checkout(bill);
                if(success == false) break;
            case 5:
                /* Cancel */
                keepGoing = false;
            default:
                break;
        }
    }

    /* Save any changes to the inventory to the file */
    this->saveInventory();
}

/* Prints the options for the transaction menu */
void Engine::printTransactionMenu() {
    cout << "Transaction" << endl;
    cout << "===========" << endl;
    cout << "[1] View Bill" << endl;
    cout << "[2] Add Item" << endl;
    cout << "[3] Remove Item" << endl;
    cout << "[4] Checkout" << endl;
    cout << "[5] Cancel" << endl;
}

/* Checkout */
bool Engine::checkout(Bill & bill) {
    /* If the bill is empty, return false as we cannot checkout on an empty tab */
    if(bill.getItems().size() == 0) {
        cout << "No items on bill!" << endl;
        this_thread::sleep_for(1s);
        return false;
    }

    string line;
    float cashTendered;
    float change;

    /* Loop until valid input is accepted */
    bool keepGoing = true;
    while(keepGoing == true) {
        /* Clear screen for cleaner UI */
        this->clearScreen();

        /* Print the bill, along with tax owed and final total */
        bill.print();
        cout << "Tax (" << fixed << setprecision(2) << bill.getSalesTax() * 100 << "%): $"
             << fixed << setprecision(2) << bill.getSubtotal() * bill.getSalesTax() << endl;
        cout << "Total: $" << fixed << setprecision(2) << bill.getTotal() << endl << endl;

        /* Options for the user */
        cout << "[1] Continue" << endl;
        cout << "[2] Cancel" << endl;
        this->printConfirmMessage();
        int choice = this->getInt();
        switch(choice) {
            /* Continue */
            case 1:
                keepGoing = false;
                break;
            /* Cancel */
            case 2:
                return false;
            default:
                break;
        }
    }

    /* Loop until valid input */
    keepGoing = true;
    while(keepGoing == true) {
        /* Clear screen for cleaner UI */
        this->clearScreen();

        /* Display amount due and request cash from customer */
        cout << "Amount due: $" << fixed << setprecision(2) << bill.getTotal() << endl;
        cout << "Cash tendered (blank for cancel): $";
        cashTendered = this->getFloat();

        /* If cash tendered is blank, then cancel the checkout */
        if(cashTendered < 0) {
            cout << "Cancelling checkout . . . " << endl;
            this_thread::sleep_for(1s);
            return false;
        }
        /* Otherwise if enough cash was tendered, continue */
        else if(cashTendered >= bill.getTotal()) {
            keepGoing = false;
        }
    }

    /* Calculate change and display */
    change = cashTendered - bill.getTotal();
    cout << "Change due: $" << fixed << setprecision(2) << change << endl;

    cout << endl << "Press <Enter> to continue . . .";
    getline(cin, line);

    /* Generate a receipt for the transaction, outputting it to a uniquely named file */
    this->generateReceipt(bill, cashTendered, change);

    /* Append sale to end of sales log */
    ofstream salesLog;
    salesLog.open(this->salesLogFullPath, fstream::out | fstream::app);
    if(salesLog.is_open()) {
        time_t theTime = time(NULL);
        struct tm * brokenTime = localtime(&theTime);
        salesLog << "\"" << setw(2) << setfill('0') << brokenTime->tm_hour << ":"
            << setw(2) << setfill('0') << brokenTime->tm_min << "\",\""
            << fixed << setprecision(2) << bill.getSubtotal() << "\",\""
            << fixed << setprecision(2) << bill.getSubtotal() * bill.getSalesTax() << "\",\""
            << fixed << setprecision(2) << bill.getTotal() << "\"" << endl;
    } else {
        cerr << "Could not open sales log for appending!" << endl;
    }
    salesLog.close();

    /* Update the inventory */
    for(auto it = bill.getItems().begin(); it != bill.getItems().end(); ++it) {
        auto iit = find(this->inventory.begin(), this->inventory.end(), *it);
        if(iit != this->inventory.end()) {
            (*iit).setQuantity((*iit).getQuantity() - (*it).getQuantity());
        }
    }

    /* If execution reaches this point, checkout was a successful, so return true */
    return true;
}

/* Generate a receipt for a bill into a uniquely named file */
void Engine::generateReceipt(Bill & bill, float cashTendered, float change) {
    ostringstream ss;
    ss << this->receiptsDirectoryPath;
    time_t theTime = time(NULL);
    struct tm * brokenTime = localtime(&theTime);
    struct tm * gmTime = gmtime(&theTime);

    int day = brokenTime->tm_mday;
    int mon = brokenTime->tm_mon + 1;
    int year = brokenTime->tm_year + 1900;
    int hour = gmTime->tm_hour + PST;
    int min = gmTime->tm_min;
    int sec = gmTime->tm_sec;
    string m = this->calcMonth(mon);
    transform(m.begin(), m.begin() + 1, m.begin(), ::toupper);

    /* Make directory if not exists */
    ostringstream dirname;
    dirname << "stat -t ";
    dirname << this->receiptsDirectoryPath + to_string(year) + "/";
    string command = dirname.str();
    if(system(command.c_str()) != 0) {
        command = "mkdir -p ";
        command += this->receiptsDirectoryPath + to_string(year) + "/";
        system(command.c_str());
    }

    dirname << setw(2) << setfill('0') << mon << "-" << m << "/";
    command = dirname.str();
    if(system(command.c_str()) != 0) {
        command = "mkdir -p ";
        command += dirname.str().substr(8);
        system(command.c_str());
    }

    ss << to_string(year) << "/";
    ss << setw(2) << setfill('0') << mon << "-" << m << "/";
    ss << setw(2) << setfill('0') << day << "-";
    ss << m << "-";
    ss << year << "__";
    ss << hour << "_";
    ss << min << "_";
    ss << sec << ".txt";

    string receiptPath = ss.str();

    /* Open the receipt file */
    ofstream receipt;
    receipt.open(receiptPath);
    if(receipt.is_open()) {
        /* Output the date */
        receipt << setw(2) << setfill(' ') << day << " "
            << m << " "
            << year << "  "
            << hour << ":"
            << min << ":"
            << sec << endl;
        receipt << setfill('=') << setw(21) << "" << endl;

        /* Print the bill */
        bill.print(receipt);
        /* Print tax and final total */
        receipt << "Tax (" << fixed << setprecision(2) << bill.getSalesTax() * 100 << "%): $"
             << fixed << setprecision(2) << bill.getSubtotal() * bill.getSalesTax() << endl;
        receipt << "Total: $" << fixed << setprecision(2) << bill.getTotal() << endl << endl;

        /* Print cash tendered and change */
        receipt << "Cash Tendered: $" << fixed << setprecision(2) << cashTendered << endl;
        receipt << "Change Due: $" << fixed << setprecision(2) << change << endl;
    } else {
        /* Print error if file could not be opened */
        cerr << "Could not open " << receiptPath << " for writing!" << endl;
        this_thread::sleep_for(1s);
    }
    receipt.close();
}
