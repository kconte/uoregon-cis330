#ifndef ENGINE_HPP_
#define ENGINE_HPP_

#include <string>
#include <vector>

#include "item.hpp"
#include "bill.hpp"

using namespace std;

class Engine {
    public:
        /* Constructor */
        Engine();

        /* Initialize engine, do resource checking, create first menu, etc. */
        void init();

        /* Starts the engine, initializing the main application logic */
        void start();

        /* Stops the engine, delete any dynamic objects, etc. */
        void stop(); 
        /* Print Company Information */
        void printCompany();

        /* User Input */
        static string & getString();
        static int getInt();
        static float getFloat();

        static void printConfirmMessage();

        /* Clear Terminal (need to find a graceful way to do so) */
        static void clearScreen();

    private:
        /* Data Members */

        /* Tracks whether the engine has gone through the setup process (void init()) */
        bool initialized;

        /* Tracks what menu engine should be using -- ALL menus get a unique ID */
        int currentMenu;

        /* Strings to store various file paths needed by the engine */
        string inventoryFilePath;
        string companySettingsFilePath;
        string salesLogDirectoryPath;
        string salesLogFullPath;
        string receiptsDirectoryPath;

        /* Strings to store company information */
        string companyName;
        string companyPhone;
        string companyEmail;
        string companyAddress;

        /* Sales Tax Percentage */
        float taxPercentage;

        /* Holds list of inventory items */
        vector<Item> inventory;

        /* Private Functions */

        /* Print Main Menu */
        void printMainMenu();

        /* Settings Methods */
        void settings();
        void saveSettings();
        void printSettingsMenu();
        void editSalesTax();
        void editCompany();
        void editFilePaths();
        void editValue(string & str, int num_lines);

        /* Inventory Methods */
        void inventoryLoop();
        void loadInventory();
        void saveInventory();
        void printInventoryMenu();
        void displayInventory();
        void addItemToInventory();
        void removeItemFromInventory();
        void editItemInInventory();
        void editPrice(float & val);
        void editQuantity(int & val);
        vector<Item>::iterator itemIn(string & name);

        /* Transaction Methods */
        void transaction();
        void printTransactionMenu();
        bool checkout(Bill & bill);
        void generateReceipt(Bill & bill, float cashTendered, float change);

        /* Sales Methods */
        void sales();
        void printSalesMenu();
        void reportRange(int startDay, int startMonth, int startYear, int endDay, int endMonth, int endYear, bool showYear, bool showAll, bool showMonthlyAvg);
        string calcMonth(int month);
        int calcMonth(string & month);
        bool validMonth(string & month, int year);
        bool validYear(string & year);
        void saveReport(ostringstream & os);

        /* Clear out cin stream */
        void clearStream();
};

#endif /* ENGINE_HPP_ */
