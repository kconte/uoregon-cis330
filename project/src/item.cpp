#include <iostream>
#include <iomanip>

#include "item.hpp"

using namespace std;

/* Default constructor */
Item::Item() {
    this->name = "";
    this->pricePerUnit = 0.0f;
    this->quantityInStock = 0;
}

/* Input constructor */
Item::Item(string str, float price, int quantity) {
    this->name = str;
    this->pricePerUnit = price;
    this->quantityInStock = quantity;
}

/* Setters */
void Item::setName(string str) { this->name = str; }

void Item::setPrice(float price) { this->pricePerUnit = price; }

void Item::setQuantity(int quantity) { this->quantityInStock = quantity; }

/* Getters */
string Item::getName() { return this->name; }

float Item::getPrice() { return this->pricePerUnit; }

int Item::getQuantity() { return this->quantityInStock; }


/* << operator overload */
ostream & operator<<(ostream & os, const Item & item) {
    os << setw(4) << setfill(' ') << item.quantityInStock << " x " << item.name
        << " @ $" << fixed << setprecision(2) << item.pricePerUnit << " ea.";
    return os;
}


/* Less than (<) comparison operator */
bool Item::operator<(const Item & rhs) {
    return (this->name < rhs.name);
}

/* == operator */
bool Item::operator==(const Item & rhs) {
    return this->name == rhs.name;
}
