#include <iostream>
#include <limits>

#include "engine.hpp"

using namespace std;

/* Get an integer from cin */
int Engine::getInt() {
    int val;
    string line = "";

    /* Read the input as a string, and parse from there */
    getline(cin, line);

    /* If the line is blank, no input was provided and we can return the error code -1 */
    if(line == "") {
        return -1;
    }

    /* If the input string contains non-numeric characters, it is an invalid input */
    if(line.find_first_not_of("-0123456789") != string::npos) {
        cout << "Invalid input." << endl;
        val = -1;
    }

    /* Otherwise, convert it to an integer */
    else {
        val = stoi(line); 
    }

    /* Return the parsed value */
    return val;
}


/* Get a floating point value from cin */
float Engine::getFloat() {
    float val = 0.0f;
    string line = "";

    /* Read the input as a string and parse from there */
    getline(cin, line);

    /* If the input is blank, no string can be parsed, return error code -1 */
    if(line == "") {
        return -1.0f;
    }

    /* If the input contains non-numeric characters, it is invalid */
    if(line.find_first_not_of("0123456789.-") != string::npos) {
        cout << "Invalid input." << endl;
        val = -1.0f;
    }

    /* Otherwise, parse the value */
    else {
        val = stof(line);
    }

    /* Return the parsed value */
    return val;
}


/* Clear the cin to avoid reading garbage */
void Engine::clearStream() {
    /* Basically, ignore all characters until the next '\n' character */
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
}
