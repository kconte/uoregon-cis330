#ifndef ITEM_HPP_
#define ITEM_HPP_

#include <string>

using namespace std;

class Item {
    public:
        /* Constructors */
        Item();
        Item(string desc, float price, int quantity);

        /* Setters */
        void setName(string str);
        void setPrice(float price);
        void setQuantity(int quantity);

        /* Getters */
        string getName();
        float getPrice();
        int getQuantity();

        /* << operator overload */
        friend ostream & operator<<(ostream & os, const Item & item);

        /* Less than comparison operator overload */
        bool operator<(const Item & rhs);

        /* == Operator overload */
        bool operator==(const Item & rhs);

    protected:
        /* Data Members */
        string name;
        float pricePerUnit;
        int quantityInStock;
};

#endif
