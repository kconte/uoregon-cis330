#include <iostream>
#include <iomanip>
#include <limits>
#include <fstream>
#include <thread>

#include "engine.hpp"

using namespace std;

/* Settings Methods */

void Engine::settings() {

    /* Flag to keep looping */
    bool keepGoing = true;

    while(keepGoing == true) {

        /* Clear screen for clean ui */
        this->clearScreen();

        /* Print the settings menu */
        printSettingsMenu();

        /* Get input from user */
        int choice = this->getInt();

        /* Perform appropriate task based on input */
        switch(choice) {
            /* Edit Sales Tax % */
            case 1:
                this->editSalesTax();
                break;

            /* Edit Company Information */
            case 2:
                this->editCompany();
                break;

            /* Edit File Paths */
            case 3:
                this->editFilePaths();
                break;

            /* Go Back */
            case 4:
                keepGoing = false;
                break;

            /* Invalid input */
            default:
                break;
        }
    }

    /* Save the changes made to the file */
    this->saveSettings();
}

/* Print the settings menu */
void Engine::printSettingsMenu() {
    cout << "Settings" << endl;
    cout << "========" << endl;
    cout << "[1] Edit Sales Tax %" << endl;
    cout << "[2] Edit Company Information" << endl;
    cout << "[3] Edit File Paths" << endl;
    cout << "[4] Go Back" << endl;

    this->printConfirmMessage();
}

/* Save changes to file */
void Engine::saveSettings() {
    /* Open the settings.txt file for writing */
    ofstream settings;
    settings.open(this->companySettingsFilePath);

    /* If it is open, the file was found */
    if(settings.is_open()) {
        /* Output all the settings, separated by lines */
        settings << this->companyName << endl;
        settings << this->companyPhone << endl;
        settings << this->companyEmail << endl;
        settings << this->companyAddress << endl;
        settings << this->inventoryFilePath << endl;
        settings << this->salesLogDirectoryPath << endl;
        settings << this->receiptsDirectoryPath << endl;
        settings << this->taxPercentage << endl;
    } else {
        /* If the file is not open, it is not found, exit with code -1 */
        cerr << "Unable to open settings file!" << endl;
        exit(-1);
    }

    /* Close the settings file */
    settings.close();
}

/* Edit the sales tax % */
void Engine::editSalesTax() {

    /* Clear the screen for a cleaner ui */
    this->clearScreen();

    /* Print current value */
    cout << "Current value: " << this->taxPercentage << "%" << endl << endl;

    /* Request new value */
    cout << "Enter new value (blank for cancel): ";   

    /* Get new value from cin */
    float value = this->getFloat();

    /* If the value is an error code, no changes will be made */
    if(value == -1.0) {
        cout << "No changes made." << endl;
        this_thread::sleep_for(1s);
        return;
    }

    /* Confirm new value */
    cout << endl << "New value is: " << fixed << setprecision(2) << value << "%" << endl << endl;
    cout << "Confirm? [Y/n] ";

    string line = "";
    getline(cin, line);

    /* If input is y/Y or blank, save changes, otherwise don't save the changes */
    if(line == "" || line == "y" || line == "Y") {
        cout << "Changes saved." << endl;
        this->taxPercentage = value;
    } else {
        cout << "Changes not saved." << endl;
    }
    this_thread::sleep_for(1s);
}

/* Edit the various file paths the application uses */
void Engine::editFilePaths() {
    /* Flag to keep looping */
    bool keepGoing = true;

    while(keepGoing == true) {
        /* Clear the screen for a cleaner ui */
        this->clearScreen();

        /* Output the menu with the current values */
        cout << "File Paths" << endl;
        cout << "==========" << endl;
        cout << "[1] Inventory Source (" << this->inventoryFilePath << ")" << endl;
        cout << "[2] Sales Logs       (" << this->salesLogDirectoryPath << ")" << endl;
        cout << "[3] Receipts         (" << this->receiptsDirectoryPath << ")" << endl;
        cout << "[4] Go Back" << endl;

        this->printConfirmMessage();

        /* Get choice from user */
        int choice = this->getInt();

        /* Perform appropriate task based on input */
        switch(choice) {
            /* Inventory Source */
            case 1:
                this->editValue(this->inventoryFilePath, 1);
                break;

            /* Sales Logs */
            case 2:
                this->editValue(this->salesLogDirectoryPath, 1);
                break;

            /* Receipts */
            case 3:
                this->editValue(this->receiptsDirectoryPath, 1);
                break;

            /* Go Back */
            case 4:
                keepGoing = false;

            /* Invalid input */
            default:
                break;
        }
    }
}
