#include <iostream>
#include <iomanip>
#include <fstream>
#include <thread>

#include "engine.hpp"

using namespace std;

/* Start the engine, main loop of program */
void Engine::start() {
    /*
       If the engine has not been initialized, settings have not been sourced
       the program cannot run.
    */
    if(!this->initialized) {
        cerr << "Engine Not Initialized Yet!" << endl;
        exit(-1);
    }


    /* flag to keep looping */
    bool keepGoing = true;

    while(keepGoing) {
        /* Clear screen for cleaner ui */
        this->clearScreen();

        /* Print the main options menu */
        this->printMainMenu();

        /* Get choice from user */
        int choice = this->getInt();

        /*
           Perform appropriate action based on user's choice, requesting input
           again on invalid input
        */
        switch(choice) {
            /* Transaction */
            case 1:
                this->transaction();
                break;

            /* Inventory */
            case 2:
                this->inventoryLoop();
                break;

            /* Sales */
            case 3:
                this->sales();
                break;

            /* Settings */
            case 4:
                this->settings();
                break;

            /* Exit */
            case 5:
                keepGoing = false;
                break;

            /* Invalid input */
            default:
                break;
        }
    }
}

/* Stop the engine, exit gracefully */
void Engine::stop() {
    this->clearScreen();
}

/* Print the main options menu */
void Engine::printMainMenu() {

    /* Print company name as menu title */
    cout << this->companyName << endl;
    cout << setw(this->companyName.length()) << setfill('=') << "" << endl;

    /* Print menu options */
    cout << "[1] Transaction" << endl;
    cout << "[2] Inventory  " << endl;
    cout << "[3] Sales      " << endl;
    cout << "[4] Settings   " << endl;
    cout << "[5] Exit       " << endl;
    
    /* Alert user as to how to submit input */
    printConfirmMessage();
}

/* Alerts the user as to how to submit input */
void Engine::printConfirmMessage() {
    cout << endl << "Press <Enter> to Confirm Selection . . ." << endl << endl;
}

/* Clear the screen for a cleaner ui */
void Engine::clearScreen() {
    /* TEMPORARY SOLUTION - NOT TESTED THOUROUGHLY */
    system("clear");
}

/* General function for changing a string value */
void Engine::editValue(string & str, int num_lines) {

    /* Clear the screen for a cleaner ui */
    this->clearScreen();


    /* Determine proper message to display based on number of lines to be evaluated */
    if(num_lines == 1) {
        cout << "Current value: " << str << endl << endl;
        cout << "Enter new value (blank for cancel): ";
    } else {
        cout << "Current value: " << endl << endl << str << endl << endl;
        cout << "Enter new value, separated by a new line (blank for cancel):" << endl << endl;
    }

    string total = "";
    string line = "";

    /* Get the requested number of lines, separating each line by the new line character */
    for(int i = 0; i < num_lines; i++) {
        getline(cin, line);
        total += line + '\n';
    }
    /* Remove the last new line character */
    total.pop_back();

    /* If the input is empty, no changes were made and the function can return having done nothing */
    if(total.find_first_not_of('\n') == string::npos) {
        cout << "No changes made." << endl;
        this_thread::sleep_for(1s);
        return;
    }

    /* Again, determine proper message to display */
    if(num_lines == 1) {
        cout << endl << "New value is: " << total << endl << endl;
    } else {
        cout << endl << "New value is:" << endl << endl << total << endl << endl;
    }

    /* Confirm input */
    cout << "Confirm? [Y/n] ";

    getline(cin, line);

    /* If the user selects y/Y or just presses enter with no input, the changes made will be saved */
    if(line == "" || line == "y" || line == "Y") {
        cout << "Changes saved." << endl;
        str = total;
    }

    /* Otherwise, the changes will not be saved */
    else {
        cout << "Changes not saved." << endl;
    }
    this_thread::sleep_for(1s);
}
