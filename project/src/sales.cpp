#include "engine.hpp"

#include <algorithm>
#include <iostream>
#include <iomanip>
#include <time.h>
#include <unistd.h>
#include <thread>
#include <fstream>

using namespace std;

void Engine::sales() {

    time_t theTime = time(NULL);
    struct tm * brokenTime = localtime(&theTime);
    int day = brokenTime->tm_mday;
    int month = brokenTime->tm_mon + 1;
    int year = brokenTime->tm_year + 1900;

    bool keepGoing = true;
    while(keepGoing == true) {
        this->clearScreen();
        this->printSalesMenu();

        int choice = this->getInt();
        if(choice == 1) {
            this->reportRange(1, month, year, day, month, year, false, true, false);
        }

        else if(choice == 2) {
            bool validInput = false;
            string line;
            int month, year;
            while(validInput == false) {
                this->clearScreen();
                cout << "Please enter a month and year (space separated, blank for cancel): ";
                getline(cin, line);
                if(line == "") { break; }
                string::size_type pos = line.find(' ');
                if(pos == string::npos) {
                    cerr << "Invalid input" << endl;
                    this_thread::sleep_for(1s);
                    continue;
                }
                transform(line.begin(), line.end(), line.begin(), ::tolower);

                string m = line.substr(0, pos);
                string y = line.substr(pos + 1);

                if(!this->validYear(y)) {
                    cerr << "Invalid year." << endl;
                    this_thread::sleep_for(1s);
                    continue;
                }

                if(!this->validMonth(m, stoi(y))) { 
                    cerr << "Invalid month." << endl;
                    this_thread::sleep_for(1s);
                    continue;
                }

                try { month = stoi(m); }
                catch (const exception & e) { month = this->calcMonth(m); }
                year = stoi(y);

                validInput = true;
            }

            if(validInput) {
                this->reportRange(1, month, year, 31, month, year, false, true, false);
            }
        }

        else if(choice == 3) {
            bool validInput = false;
            int year;
            while(validInput == false) {
                this->clearScreen();
                cout << "Please enter a year (blank for cancel): ";
                year = this->getInt();

                if(year == -1) { break; }

                string rep = to_string(year);
                if(this->validYear(rep))
                    validInput = true;
                else {
                    cerr << "Please enter a valid year!" << endl;
                    this_thread::sleep_for(1s);
                }
            }

            if(validInput) {
                this->reportRange(1, 1, year, 31, 12, year, true, false, true);
            }

        }

        else if(choice == 4) {
            keepGoing = false;
        }
    }
}

/* Print the main sales menu */
void Engine::printSalesMenu() {
    cout << "Sales" << endl;
    cout << "=====" << endl;
    cout << "[1] Current Month" << endl;
    cout << "[2] Select Month" << endl;
    cout << "[3] Select Year" << endl;
    cout << "[4] Go Back" << endl;

    this->printConfirmMessage();
}

void Engine::reportRange(int startDay, int startMonth, int startYear, int endDay, int endMonth, int endYear, bool showYear, bool showAll, bool showMonthAvg) {
    this->clearScreen();
    string line;

    if(startYear > endYear) {
        cerr << "Invalid date range. Starting year is greater than ending year." << endl;
        cerr << "Press <Enter> to continue . . . ";
        getline(cin, line);
        return;
    } else if(startMonth > endMonth) {
        cerr << "Invalid date range. Starting month is greater than ending month." << endl;
        cerr << "Press <Enter> to continue . . . ";
        getline(cin, line);
        return;
    } else if(startDay > endDay) {
        cerr << "Invalid date range. Starting day is greater than ending day." << endl;
        cerr << "Press <Enter> to continue . . . ";
        getline(cin, line);
        return;
    }

    cout << "Generating Report . . . " << endl;


    double avgPerDay = 0.0F;
    double avgPerMonth = 0.0F;
    double total = 0.0F;
    double monthTotal = 0.0F;
    double yearTotal = 0.0F;
    int numDays = 0;
    int numMonths = 0;
    bool validMonth = false;

    string currentFile;

    ostringstream output;
    string sMon = this->calcMonth(startMonth);
    transform(sMon.begin(), sMon.begin() + 1, sMon.begin(), ::toupper);
    string eMon = this->calcMonth(endMonth);
    transform(eMon.begin(), eMon.begin() + 1, eMon.begin(), ::toupper);
    output << setw(2) << setfill(' ') << startDay << " " << sMon
        << " " << startYear << " -> " << endDay << " " << eMon
        << " " << endYear << endl;
    output << "==========================" << endl;

    ostringstream ss;

    ifstream file;

    for(int year = startYear; year <= endYear; year++) {

        yearTotal = 0.0F;

        for(int month = (year == startYear ? startMonth : 1); month <= (year == endYear ? endMonth : 12); month++) {

            validMonth = false;
            
            monthTotal = 0.0F;

            for(int day = (month == startMonth ? startDay : 1); day <= (month == endMonth ? endDay : 31); day++) {

                ss.str(string());

                string m = this->calcMonth(month);
                transform(m.begin(), m.begin() + 1, m.begin(), ::toupper);
                ss << this->salesLogDirectoryPath
                    << year << "/"
                    << setw(2) << setfill('0') << month << "-" << m << "/"
                    << day;

                currentFile = ss.str();

                file.open(currentFile);

                string line;

                if(file.is_open()) {
                    if(validMonth == false)
                        numMonths++;
                    validMonth = true;
                    numDays++;
                    while(getline(file, line)){
                        /* skip first record */
                        auto start = line.find('"');
                        auto end = line.find('"', start + 1);
                        start = line.find('"', end + 1);
                        end = line.find('"', start + 1);
                        monthTotal += stof(line.substr(start + 1, end - 1));
                    }
                }

                file.close();
            }

            string mon = this->calcMonth(month);
            transform(mon.begin(), mon.begin() + 1, mon.begin(), ::toupper);
            output << mon << " " << year << " Total: $"
                << fixed << setprecision(2) << monthTotal << endl;

            yearTotal += monthTotal;
        }

        if(showYear) {
            output << endl << year << " Total: $"
                << fixed << setprecision(2) << yearTotal << endl;
        }

        total += yearTotal;
    }

    avgPerDay = total / numDays;
    output << endl << endl << "Daily Average: $"
        << fixed << setprecision(2) << avgPerDay << endl;

    if(showMonthAvg) {
        avgPerMonth = total / numMonths;
        output << "Monthly Average: $"
            << fixed << setprecision(2) << avgPerMonth << endl;
    }

    if(showAll) {
        output << endl << endl <<  "Total Sales: $"
            << fixed << setprecision(2) << total << endl;
    }

    this->clearScreen();

    cout << output.str() << endl;

    cout << "Save report [y/N]? ";
    getline(cin, line);

    if(line == "y" || line == "Y") {
        this->saveReport(output);
    }
}

string Engine::calcMonth(int month) {
    switch(month) {
        case 1:  return "jan";
        case 2:  return "feb";
        case 3:  return "mar";
        case 4:  return "apr";
        case 5:  return "may";
        case 6:  return "jun";
        case 7:  return "jul";
        case 8:  return "aug";
        case 9:  return "sep";
        case 10: return "oct";
        case 11: return "nov";
        case 12: return "dec";
        default: return "";
    }
}

int Engine::calcMonth(string & month) {
    string tmp = month.substr(0, 3);
    transform(tmp.begin(), tmp.end(), tmp.begin(), ::tolower);
    if(tmp == "jan") return 1;
    else if(tmp == "feb") return 2;
    else if(tmp == "mar") return 3;
    else if(tmp == "apr") return 4;
    else if(tmp == "may") return 5;
    else if(tmp == "jun") return 6;
    else if(tmp == "jul") return 7;
    else if(tmp == "aug") return 8;
    else if(tmp == "sep") return 9;
    else if(tmp == "oct") return 10;
    else if(tmp == "nov") return 11;
    else if(tmp == "dec") return 12;
    else return -1;
}

bool Engine::validMonth(string & month, int year) {
    int m = 0;
    try { m = stoi(month); }
    catch(const exception & e) {}

    bool validString = false;

    ostringstream ss;
    ss << "stat -t " << this->salesLogDirectoryPath << year << "/";
    string command = "";
    if(m == 0) {
        if(month.rfind("jan", 0) == 0 ||
                month.rfind("feb", 0) == 0 ||
                month.rfind("mar", 0) == 0 ||
                month.rfind("apr", 0) == 0 ||
                month.rfind("may", 0) == 0 ||
                month.rfind("jun", 0) == 0 ||
                month.rfind("jul", 0) == 0 ||
                month.rfind("aug", 0) == 0 ||
                month.rfind("sep", 0) == 0 ||
                month.rfind("oct", 0) == 0 ||
                month.rfind("nov", 0) == 0 ||
                month.rfind("dec", 0) == 0) {
            string mon = month.substr(0, 3);
            transform(mon.begin(), mon.begin() + 1, mon.begin(), ::toupper);
            ss << setw(2) << setfill('0') << this->calcMonth(month) << "-" << mon;
        }
    }
    else if(m >= 1 && m <= 12) {
        string mon = this->calcMonth(m);
        transform(mon.begin(), mon.begin() + 1, mon.begin(), ::toupper);
        ss << setw(2) << setfill('0') << m << "-" << mon;
    }

    else { return false; }

    command = ss.str();
    return system(command.c_str()) == 0;
}

bool Engine::validYear(string & year) {
    try { stoi(year); }
    catch(const exception & e) { return false; }
    string command = "";
    command += "stat -t " + this->salesLogDirectoryPath + year + " > /dev/null";
    return system(command.c_str()) == 0;
}

void Engine::saveReport(ostringstream & os) {
    time_t theTime = time(NULL);
    struct tm * brokenTime = localtime(&theTime);

    int day = brokenTime->tm_mday;
    string mon = this->calcMonth(brokenTime->tm_mon + 1);
    int year = brokenTime->tm_year + 1900;

    ostringstream ss;
    ss << "./" << setw(2) << setfill('0') << day << "-" << mon << "-" << year << ".txt";
    string fileName = ss.str();

    string Mon = mon;
    transform(Mon.begin(), Mon.begin() + 1, Mon.begin(), ::toupper);

    ofstream file;
    file.open(fileName);
    if(file.is_open()) {
        file << os.str();
        file << endl << "Report Generated on: "
            << setw(2) << setfill(' ') << day << " "
            << Mon << " "
            << year << endl;
    }
    file.close();

    cout << "Report saved to: '" << fileName << "'." << endl;
    this_thread::sleep_for(3s);
}
