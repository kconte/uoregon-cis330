#include <iostream>

#include "engine.hpp"

using namespace std;

/* Print Company Information */
void Engine::printCompany() {
    /* If the engine has not been initialized, the company information has yet to be sourced */
    if(this->initialized == false) {
        cerr << "Engine not yet initialized!" << endl;
        exit(-1);
    }

    /* Print the name, phone, email, and address on separate lines */
    cout << this->companyName << endl
        << this->companyPhone << endl
        << this->companyEmail << endl << endl;

    cout << this->companyAddress << endl << endl;
}

/* Edit portions of the company's information */
void Engine::editCompany() {
    
    /* Flag to keep looping */
    bool keepGoing = true;

    while(keepGoing == true) {
        /* Clear screen for easier ui */
        this->clearScreen();

        /* Print menu title */
        cout << "Edit Company" << endl;
        cout << "============" << endl << endl;

        /* Display current company information */
        this->printCompany();

        /* Print menu options */
        cout << "[1] Name" << endl;
        cout << "[2] Phone Number" << endl;
        cout << "[3] Email" << endl;
        cout << "[4] Address" << endl;
        cout << "[5] Go Back" << endl;

        /* Alert user as to how to input a choice */
        this->printConfirmMessage();

        /* Get choice from user */
        int choice = this->getInt();

        /* Select proper item based on user's input, clearing and requesting again if invalid input */
        switch(choice) {
            /* Name */
            case 1:
                this->editValue(this->companyName, 1);
                break;

            /* Phone Number */
            case 2:
                this->editValue(this->companyPhone, 1);
                break;

            /* Email Address */
            case 3:
                this->editValue(this->companyEmail, 1);
                break;

            /* Physical Address */
            case 4:
                this->editValue(this->companyAddress, 3);
                break;

            /* Go Back */
            case 5:
                keepGoing = false;

            /* Invalid input */
            default:
                break;
        }
    }
}
