#include "engine.hpp"

#include <algorithm>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <time.h>
#include <unistd.h>

using namespace std;

/* Default constructor */
Engine::Engine() {
    /* Default values for data members */

    /* Not initialized */
    this->initialized = false;

    /* Default Menu is Main Menu */
    this->currentMenu = 0;

    /* All paths are to be loaded from settings, except settings path of course */
    this->inventoryFilePath = "";
    this->salesLogDirectoryPath = "";
    this->salesLogFullPath = "";
    this->receiptsDirectoryPath = "";

    /* Company information strings to loaded from settings */
    this->companyName = "";
    this->companyPhone = "";
    this->companyEmail = "";
    this->companyAddress = "";

    /* Settings path MUST be in directory of executable */
    this->companySettingsFilePath = "./assets/settings.txt";

    /* Tax Percentage loaded from settings */
    this->taxPercentage = 0.0;
}

/* init, checks for application resources (settings file, inventory file, etc.) */
void Engine::init() {
    /* Load Settings */
    ifstream settings;
    settings.open(this->companySettingsFilePath);

    if(settings.is_open()) {
        /* Company Settings */
        getline(settings, this->companyName);
        getline(settings, this->companyPhone);
        getline(settings, this->companyEmail);
        string line1, line2, line3;
        getline(settings, line1);
        getline(settings, line2);
        getline(settings, line3);
        this->companyAddress = line1 + '\n' + line2 + '\n' + line3;

        /* File Paths */
        getline(settings, this->inventoryFilePath);
        getline(settings, this->salesLogDirectoryPath);
        getline(settings, this->receiptsDirectoryPath);

        settings >> this->taxPercentage;
    }

    /* If engine cannot find a settings file, go through a set up process */
    else {
        cout << "Could not find Settings File!" << endl;
        exit(-1);
    }

    /* Close Settings File - No longer needed */
    settings.close();

    /* Load inventory into memory */
    this->loadInventory();

    /* Make the sales log for current day */
    time_t theTime = time(NULL);
    struct tm * brokenTime = localtime(&theTime);

    int year = brokenTime->tm_year + 1900;
    int month = brokenTime->tm_mon + 1;
    int day = brokenTime->tm_mday;

    ostringstream ss;
    string mon = this->calcMonth(month);
    transform(mon.begin(), mon.begin() + 1, mon.begin(), ::toupper);
    ss << this->salesLogDirectoryPath << to_string(year) << "/";
    ss << setw(2) << setfill('0') << month << "-" << mon << "/";

    string command = "stat -t " + ss.str();
    if(system(command.c_str()) != 0) {
        command = "mkdir -p " + ss.str();
        system(command.c_str());
    }

    ss << setw(2) << setfill(' ') << day;

    this->salesLogFullPath = ss.str();

    /* If execution reaches this point, the engine is ready to use! */
    this->initialized = true;
}
