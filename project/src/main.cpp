#include <algorithm>
#include <iostream>
#include <iterator>

#include "engine.hpp"
#include "item.hpp"

using namespace std;

int main() {

    Engine engine;
    engine.init();

    engine.start();

    engine.stop();

    return 0;
}
