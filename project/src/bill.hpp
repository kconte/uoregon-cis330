#ifndef BILL_HPP_
#define BILL_HPP_

#include "item.hpp"
#include <vector>

class Bill {
    public:
        Bill();
        Bill(float tax);

        void addItem(vector<Item> & inventory);
        void removeItem();
        void editItem();
        void print(ostream & os);
        void print();

        float getSubtotal() { return this->subtotal; }
        float getTotal() { return this->total; }
        float getSalesTax() { return this->salesTax; }
        vector<Item> & getItems() { return this->items; }

    private:
        vector<Item> items;
        float subtotal;
        float total;
        float salesTax;

        void updateTotals();

        int selectItem();
        int selectItem(vector<Item> & items);
};

#endif /* BILL_HPP_ */
