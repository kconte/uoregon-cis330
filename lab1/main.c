/*
   Kevin Conte
   Lab 1
   11 January 2018
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Defines a Student struct */
typedef struct {
    char name[32]; /* Maximum Name Length: 31 */
    int scores[16]; /* Maximum Scores: 16 */
    float avg;
} Student;


int main() {

    char line[127];
    char * item;
    Student students[300];

    int student_index = 0;
    printf("\"NAME\",\"AVG\"\n");
    while(fgets(line, 64, stdin) != NULL) {

        /* Parse each item by comma */
        item = strtok(line, ",");

        /* First item parsed is assumed to be the name of the student */
        strcpy(students[student_index].name, item);

        /* Next item parsed is the first score */
        /* If no score is parsed, the following while loop will not execute, resulting in an avg score of 0.0 */
        item = strtok(NULL, ",");
        int score_index = 0; /* Keep track of which score we are on (used in determining averages) */
        while(item != NULL) {
            /* Scans an integer from the string parsed with strtok() into the current student's scores array */
            sscanf(item, "%d", &students[student_index].scores[score_index++]);

            /* Next score is parsed, process repeated until end of line */
            item = strtok(NULL, ",");
        }

        /* Avg score is assumed to be 0. */
        students[student_index].avg = 0.0f;
        /* Add all scores up into the avg */
        for(int i = 0; i < score_index; i++) {
            students[student_index].avg += (float)students[student_index].scores[i];
        }
        /* Divide avg by number of scores to get accurate avg */
        students[student_index].avg /= (float)score_index;

        /* Report findings to stdout, rounding to two decimal places */
        fprintf(stdout, "\"%s\",\"%.2f\"\n", students[student_index].name, students[student_index].avg);

        /* Increment current student counter */
        student_index++;
    }

    /* Free the memory used by "char * item;" */
    free(item);

    /* Return with success code */
    return 0;
}
