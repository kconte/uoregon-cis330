#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include <map>
#include <chrono>

#include "matplotlibcpp.h"

using namespace std;
namespace plt = matplotlibcpp;

/* Prints the values of a vector */
void printVector(const vector<int> & v) {
    for(vector<int>::const_iterator it = v.begin(); it != v.end(); ++it) {
        cout << *it << ' ';
    }
    cout << endl;
}

void plotVector(const vector<int> & v, const char * title, const char * dest) {
    plt::clf();
    plt::plot(v);
    plt::title(title);
    plt::save(dest);
}

void outputVector(const vector<int> & v, const char * title, const char * dest) {
    cout << title << ":" << endl;
    printVector(v);
    plotVector(v, title, dest);
}

int main() {

    /* Instantiate new vector of size 15 */
    vector<int> v(100);

    /* RNG Dependencies */
    random_device rd;
    mt19937 engine { rd() };
    uniform_int_distribution<int> dist {1, 100};

    /* Fill v with random values */
    generate(v.begin(), v.end(), [&dist, &engine]() { return dist(engine); });

    /* Output v */
    outputVector(v, "v Original", "./vOrig.png");

    /* Sort v */
    sort(v.begin(), v.end());

    /* Output v */
    outputVector(v, "v Sorted", "./vSorted.png");

    /* Copy v into vCopy */
    vector<int> vCopy(v.size());
    copy(v.begin(), v.end(), vCopy.begin());

    /* Output vCopy */
    outputVector(vCopy, "vCopy Original", "./vCopyOrig.png");

    /* Shuffle  vCopy */
    random_shuffle(vCopy.begin(), vCopy.end());

    /* Output vCopy */
    outputVector(vCopy, "vCopy Shuffled", "./vCopyShuffled.png");

    /* Time some things */

    for(unsigned long i = 1; i <= 1000000000LL; i *= 10) {
    
        cout << "Time to sum the values of a vector of size " << i << ":" << endl;
        vector<int> a(i, 1);

        /* Indexing */
        auto start = chrono::high_resolution_clock::now();
        unsigned long long sum = 0;
        for(unsigned long int i = 0; i < a.size(); i++) {
            sum += a.at(i);
        }
        auto end = chrono::high_resolution_clock::now();
        chrono::duration<double> diff = end - start;
        cout << "Indexing: " << diff.count() << " s" << endl;

        /* Iterator */
        start = chrono::high_resolution_clock::now();
        sum = 0;
        for(auto it = a.begin(); it != a.end(); ++it) {
            sum += *it;
        }

        end = chrono::high_resolution_clock::now();
        diff = end - start;
        cout << "Iterators: " << diff.count() << " s" << endl;

        /* Algorithms */
        start = chrono::high_resolution_clock::now();
        sum = 0;
        sum = accumulate(a.begin(), a.end(), 0);
        end = chrono::high_resolution_clock::now();
        diff = end - start;
        cout << "Algorithms: " << diff.count() << " s" << endl;

        cout << endl << endl;
    }


    /* Exit gracefully */
    return 0;
}
